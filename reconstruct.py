#!/usr/bin/env python3

import argparse as ArgParser

import os
import sys
import csv

import random
import numpy as np

from drawing.plotter import SinglePlot, MultiPlotter
from drawing.environment import Environment
from drawing.room import Room
from drawing.physicalBody import Pendulum
from drawing.primatives.prism import Prism

from OpenGL.GL import *

import constants as Consts
import utils as Utils

useCeil = 0b1111111 & 0b0
useYNums = 0b11111 & 0b0
environment = 0
bobShape = 0
multiPlotter = 0
simData, metaVars = {}, {}

def parseArgs():
	parser = ArgParser.ArgumentParser()
	parser.add_argument('-i', '--input', metavar = 'input', help = 'When this is used, it is infered if you are passing a directory or specific input file.  ' +
										'If used with -d, this is treated as the file within the directory.  If used with -f, this is ignored.')
	parser.add_argument('-f', '--file', metavar = 'file', help = 'The specific file to recreate.')
	parser.add_argument('-d', '--directory', metavar = 'directory', help = 'The directory from which to choose a random input from.  If used with -f, this is ignored.')
	return parser.parse_args()

def step(time):
	global bobBody, mutliPlotter

	# Step the bobShape by assigning it the values parsed from the csv at this time
	theta, omega, alpha = simData[time][Consts.THETA_3D_CSV], simData[time][Consts.OMEGA_3D_CSV], simData[time][Consts.ALPHA_3D_CSV]

	bobShape.setRotation(theta, isRadians = False)
	pos = Utils.polarToCartesian(metaVars[Consts.ARM_LENGTH_CSV], bobShape.getTrigs())
	np.subtract(Consts.CONNECTION_POINT, pos, pos)
	bobShape.setPosition(pos)

	if environment.halted and environment.singleStepFlag:
		print("Time:", environment.time, " theta:", theta, " omega:", omega, " alpha:", alpha)

	if not multiPlotter.finished:
		bobVars = simData[time]
		for bobVarName, bobVarVal in bobVars.items():
			if bobVarName in multiPlotter.plotIds:
				multiPlotter.appendDataById(time, bobVarVal, bobVarName)
		multiPlotter.appendDataById(time, Utils.to1d(theta), Consts.THETA_1D_CSV)
#		multiPlotter.appendDataById(time, Utils.to1d(omega), Consts.OMEGA_1D_CSV)
#		multiPlotter.appendDataById(time, Utils.to1d(alpha), Consts.ALPHA_1D_CSV)

		for metaVarName, metaVarVal in metaVars.items():
			if metaVarName in multiPlotter.plotIds:
				multiPlotter.appendDataById(time, metaVarVal, metaVarName)

	if time == len(simData)-1:
		environment.addOneTimePostLoop(finish)


def draw():
	# Could just draw to a translated object position, but this is a good sanity check
	ep = Utils.polarToCartesian(metaVars[Consts.ARM_LENGTH_CSV], bobShape.getTrigs())
	np.subtract(Consts.CONNECTION_POINT, ep, ep)

	glLineWidth(2.0)
	glColor(255, 0, 0)
	glBegin(GL_LINES)
	glVertex3f(*Consts.CONNECTION_POINT)
	glVertex3f(*ep)
	glEnd()

	# Just for style points, draw nodes at the ends of the arm
	glBegin(GL_POINTS)
	glVertex3f(*Consts.CONNECTION_POINT)
	glVertex3f(*ep)
	glEnd()


def finish(t):
	global environment, multiPlotter
	environment.halted = True
	multiPlotter.finish()


def initPlotters():
	global multiPlotter

	ids = Pendulum.getValueIds()

	normedLines = ['c-']
	normedLegend = [("cyan", "Normalized")]

	constLines = ['m-']
	constLegend = [("magenta", "Constant")]

#	theta3d = SinglePlot(id = ids[0], yLabel = "Theta", dimens = 3, toDraw = SinglePlot.LEGEND | SinglePlot.Y_LABEL | (SinglePlot.Y_NUMS & useYNums))
	theta3d = SinglePlot(id = ids[0], yLabel = "Theta", dimens = 3, toDraw = SinglePlot.LEGEND | SinglePlot.Y_LABEL | SinglePlot.Y_NUMS)
#	theta1d = SinglePlot(id = ids[1], yLabel = "Theta", dimens = 1, toDraw = SinglePlot.LEGEND | SinglePlot.Y_LABEL | (SinglePlot.Y_NUMS & useYNums), lines = normedLines, legend = normedLegend)
	theta1d = SinglePlot(id = ids[1], yLabel = "Theta", dimens = 1, toDraw = SinglePlot.LEGEND | SinglePlot.Y_LABEL | SinglePlot.Y_NUMS | SinglePlot.X_NUMS | SinglePlot.X_LABEL, lines = normedLines, legend = normedLegend)

#	omega3d = SinglePlot(id = ids[2], yLabel = "Omega", dimens = 3, toDraw = SinglePlot.Y_LABEL | (SinglePlot.Y_NUMS & useYNums))
#	omega1d = SinglePlot(id = ids[3], yLabel = "Omega", dimens = 1, toDraw = SinglePlot.Y_LABEL | (SinglePlot.Y_NUMS & useYNums), lines = normedLines)

#	alpha3d = SinglePlot(id = ids[4], yLabel = "Alpha", dimens = 3, toDraw = SinglePlot.Y_LABEL | (SinglePlot.Y_NUMS & useYNums) | SinglePlot.X_LABEL | SinglePlot.X_NUMS)
#	alpha1d = SinglePlot(id = ids[5], yLabel = "Alpha", dimens = 1, toDraw = SinglePlot.Y_LABEL | (SinglePlot.Y_NUMS & useYNums) | SinglePlot.X_LABEL | SinglePlot.X_NUMS, lines = normedLines)

#	gravity = SinglePlot(id = Consts.GRAVITY_CSV, yLabel = "Gravity", dimens = 1, toDraw = SinglePlot.LEGEND | SinglePlot.Y_LABEL | SinglePlot.Y_NUMS, lines = constLines, legend = constLegend)
#	mass = SinglePlot(id = ids[6], yLabel = "Bob Mass", dimens = 1, toDraw = SinglePlot.Y_LABEL | SinglePlot.Y_NUMS, lines = constLines)
#	pendulumLength = SinglePlot(id = ids[7], yLabel = "Pendulum Length", dimens = 1, toDraw = SinglePlot.Y_LABEL | SinglePlot.Y_NUMS | SinglePlot.X_LABEL | SinglePlot.X_NUMS, lines = constLines)

#	plotters = [
#		[theta3d, theta1d, gravity],
#		[omega3d, omega1d, mass],
#		[alpha3d, alpha1d, pendulumLength]
#	]

	plotters = [[theta3d], [theta1d]]

	multiPlotter = MultiPlotter(plotters)


def parseNpArray(npString):
	strVals = npString[1:-1].split()
	floatVals = [float(strVal) for strVal in strVals]
	return np.array(floatVals)


def reconstruct(fileBase):
	global environment, bobShape, simData, metaVars

	# Get everything from the sim and meta csvs
	with open(fileBase + ".csv", newline = '') as csvFile:
		reader = csv.DictReader(csvFile, delimiter = ',', quotechar = '"', quoting = csv.QUOTE_ALL)

		# Parse each row into a dict of this form:
		# {times...: {varNames...: varValues...}}
		# Skip the first row because its just the headers
		for row in reader:
			vars = {}
			for varName, varVal in row.items():
				if varName == Consts.TIME_CSV:
					continue
				# Parse the string into an np.array
				vars[varName] = parseNpArray(varVal)
			simData[int(row[Consts.TIME_CSV])] = vars

	with open(fileBase + "." + Consts.META_VARS_CSV + ".csv", newline = '') as csvFile:
		reader = csv.DictReader(csvFile, delimiter = ',', quotechar = '"', quoting = csv.QUOTE_ALL)

		# The meta vars is simply the second row
		# Just parse each variable to a float
		for row in reader:
			for varName, varVal in row.items():
				if varVal[0] == "[":
					metaVars[varName] = parseNpArray(varVal)
				else:
					# Parse the value to a float and add it to the metaVars
					metaVars[varName] = float(varVal)

	room = Room(Consts.ROOM_DIMENS, faceColor = [0, 0, 255], faces = useCeil & Prism.TOP, shouldDraw = Prism.VERTICES | Prism.EDGES)

	# Apply a perminant transformation to move the bob down, essentially setting the position as the top corner, not bottom center
	permTrans = np.array([0, 0, -Consts.BOB_DIMENS[2]])

	rr = random.random()
	rg = random.random()
	rb = random.random()
	bobShape = Prism(Consts.BOB_DIMENS, faces = Prism.ALL, faceColor = (rr, rg, rb), center = Prism.X | Prism.Y, permTrans = permTrans, isRadians = False, shouldDraw = Prism.EDGES | Prism.VERTICES)

	initPlotters()


	environment = Environment(name = "Pendulum", size = Consts.WINDOW_DIMENS, drawFunction = draw)
	environment.addPreStep(step)

	environment.registerRoom(room)
	environment.registerObject(bobShape)

	environment.activate()


def stripToBase(file):
	if ("." + Consts.META_VARS_CSV + ".csv") in file:
		return file[:-1 * len("." + Consts.META_VARS_CSV + ".csv")]
	elif ".csv" in file:
		return file[:-4]
	return file


if __name__ == "__main__":
	args = parseArgs()

	# Check that atleast one of -f, -i, -d are supplied
	if args.file is None and args.input is None and args.directory is None:
		print("Error: Atleast one flag must be used to point the reconstructive engine at a file.")
		sys.exit()

	# If a file is supplied, strip it to its base and we are good to go
	# Check that it exists as well
	if not args.file is None:
		if not os.path.isfile(args.file):
			print("Error: The file '" + args.file + "' does not exist.")
			sys.exit()

		fileBase = stripToBase(args.file)

	# If a directory is supplied, no file, and...
	#	an input then the file is join(dir, in)
	#	no input then the file is randFile(dir)
	# First though, check if the directory even exists
	elif not args.directory is None:
		if not os.path.isdir(args.directory):
			print("Error: The directory '" + args.directory + "' does not exist.")
			sys.exit()

		if args.input is not None:
			fileBase = stripToBase(os.path.join(args.directory, args.input))
		else:
			print("Selecting a random file from the directory '" + args.directory + "'.")
			randFile = random.choice([f for f in os.listdir(args.directory) if ".csv" in f])
			fileBase = stripToBase(os.path.join(args.directory, randFile))

	# If only an input is supplied, interpret it as a directory or file
	elif not args.input is None:
		if os.path.isdir(args.input):
			print("Selecting a random file from the directory '" + args.input + "'.")
			randFile = random.choice([f for f in os.listdir(args.input) if ".csv" in f])
			fileBase = stripToBase(os.path.join(args.input, randFile))
		elif os.path.exists(args.input):
			fileBase = stripToBase(args.input)
		else:
			print("Error: Input file '" + args.input + "' cannot be interpreted as a file or directory.")
			sys.exit()

	print("Reconstructing the file set '" + fileBase + "' (.csv and ." + Consts.META_VARS_CSV + ".csv).")
	reconstruct(fileBase)
