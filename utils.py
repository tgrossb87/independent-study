import math
import numpy as np

import constants as Consts

def check3dList(check, varName, methodName, className, requireNpArray = True, allowNone = False):
	if requireNpArray and not isinstance(check, np.ndarray):
		reportVarError("is not a numpy array", varName, methodName, className)
		return False
	elif not isinstance(check, (np.ndarray, list)):
		reportVarError("is not a list", varName, methodName, className)
		return False

	if not len(check) == 3:
		reportVarError("has a length of %i, length of 3 required" % len(check), varName, methodName, className)
		return False

	if not allowNone:
		for el in check:
			if el is None:
				reportVarError("contains None element", varName, methodName, className)
				return False

	return True


def checkColorList(check, varName, methodName, className):
	if not isinstance(check, (np.ndarray, list, tuple)):
		reportVarError("is not a list or tuple", varName, methodName, className)
		return False

	if not len(check) == 3:
		reportVarError("has a length of %i, length of 3 required" % len(check), varName, methodName, className)
		return False

	for el in check:
		if el is None:
			reportVarError("contains None element", varName, methodName, className)
			return False

	return True


def checkBitwise(bits, length, varName, methodName, className):
	if bits.bit_length() > length:
		reportVarError("is %i bits long, length of %i or less is required" % (bits.bit_length(), length), varName, methodName, className)
		return False

	return True



def reportVarError(err, varName, methodName, className):
	reportError("'%s' %s (in method '%s' of class '%s')" % (varName, err, methodName, className))


def reportError(err):
	print("Error: %s" % err)


def bitwiseFromBool(condition, length):
	if condition:
		return (0b0 << length) - 1
	else:
		return 0b0


def to1d(vec):
	normed = np.linalg.norm(vec)
	return normed if vec[0] >= 0 else normed * -1


def bitSetAt(bits, at):
	return bool(bits & (0b1 << at))


def bitVarSet(bits, var):
	return bool(bits & var)


def zeros3d():
	return filledArray(3)


def filledArray(dimen, element = 0.0):
	return np.array([element for c in range(dimen)])


def fromNormal(theta = None, trig = None, isRadians = True, gravity = Consts.GRAVITY):
	# Allow optimization by avoiding trig calls for known angles
	if not trig == None:
		input = np.array([gravity * trig[Consts.COS], -1 * gravity * trig[Consts.SIN]])
		normalVec = rotateVector(input, trig = trig)
	elif not theta == None:
		if not isRadians:
			theta = math.radians(theta)
		input = np.array([gravity * math.sin(theta), -1 * gravity * math.cos(theta)])
		normalVec = rotateVector(input, theta = theta, isRadians = True)
	else:
		print("The method fromNormal requires either theta or trig definitions, but neither were supplied")
		return None
	return np.insert(normalVec, 1, 0.0)


# Returns input (2d vector) rotated by radians radians on each axis
def rotateVector(input, theta = None, trig = None, isRadians = True):
	# Allow optimization by avoiding trig calls for known angles
	if not trig == None:
		return input * trig[Consts.COS]
	elif not theta == None:
		if not isRadians:
			return input * math.cos(math.radians(theta))
		return input * math.cos(theta)
	else:
		print("The method rotateVector requires either theta or trig definitions, but neither were supplied")
		return None


def checkSavedData(savedData, *checkData):
	if not len(savedData) == len(checkData):
		return False
	for c in range(len(checkData)):
		if isinstance(savedData[c], (list, np.ndarray)):
			if not np.array_equal(savedData[c], checkData[c]):
				return False
		elif not (savedData[c] == checkData[c]):
			return False
	return True


def polarToCartesian(r, trig):
	# The definition of the angles are a little messed up, so x and y are switched when changing from polar to cartesian
	x = r * trig[0][Consts.SIN] * trig[2][Consts.SIN]
	y = r * trig[0][Consts.SIN] * trig[2][Consts.COS]
	z = r * trig[0][Consts.COS]
	return np.array([-x, -y, z])


def constructTrigsDef(thetas):
	trigs = []
	for theta in thetas:
		trigs.append({Consts.SIN: math.sin(theta), Consts.COS: math.cos(theta)})
	return trigs


def rotatePointAbout(point, about, trigs):
	origPoint = np.subtract(point, about).reshape((3, 1))
	xc, yc, zc = trigs[0][Consts.COS], trigs[1][Consts.COS], trigs[2][Consts.COS]
	xs, ys, zs = trigs[0][Consts.SIN], trigs[1][Consts.SIN], -trigs[2][Consts.SIN] # Invert zs because everything is hacked together

	xRotMatrix = np.array([
		[1, 0, 0],
		[0, xc, -xs],
		[0, xs, xc]
	])

	yRotMatrix = np.array([
		[yc, 0, ys],
		[0, 1, 0],
		[-ys, 0, yc]
	])

	zRotMatrix = np.array([
		[zc, -zs, 0],
		[zs, zc, 0],
		[0, 0, 1]
	])

	prime = np.matmul(xRotMatrix, origPoint)
	doublePrime = np.matmul(yRotMatrix, prime)
	triplePrime = np.matmul(zRotMatrix, doublePrime)

	return np.add(about, triplePrime.reshape(3))

