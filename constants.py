import math
import numpy as np

ROOM_DIMENS = np.array([500, 500, 500])
CONNECTION_POINT = np.array([0, 0, ROOM_DIMENS[2]/2])

BOB_DIMENS = np.array([50, 50, 50])

# 0.0375 $ 46.0 $ 11.0 $ 1 $ 295

#BOB_MASS = 2.5
BOB_MASS = 1

#ARM_LENGTH = 250
ARM_LENGTH = 295

SIM_LENGTH = 500 # In number of frames

WINDOW_DIMENS = (700, 700)

#INIT_THETA = np.array([45., 0, 45.])
INIT_THETA = np.array([46., 0, 11.])

INIT_THETA_R = np.array([math.radians(INIT_THETA[0]), math.radians(INIT_THETA[1]), math.radians(INIT_THETA[2])])

# Define the cos and sin of RAMP_THETA_R here so they don't have to be computed over and over again
COS = 'cos'
SIN = 'sin'
INIT_TRIG = [
	{COS: math.cos(INIT_THETA_R[0]), SIN: math.sin(INIT_THETA_R[0])},
	{COS: 1, SIN: 0}, # Rotation should only be defined in x and z
	{COS: math.cos(INIT_THETA_R[2]), SIN: math.sin(INIT_THETA_R[2])}
]

#GRAVITY = 0.05
GRAVITY = 0.0375


ZOOM_SPEED = 50
PAN_SPEED = 10
ROTATE_SPEED = 5

MODEL_PERSPECTIVE = False

INITIAL_ZOOM = -1150
INITIAL_HX = -45.0
INITIAL_HZ = -45.0
INITIAL_HX = -90.0
INITIAL_HZ = 0.0

KEY_SNAP_BACK = b'r'
KEY_ZOOM_IN = b'z'
KEY_ZOOM_OUT = b'x'
KEY_MOVE_LEFT = b'a'
KEY_MOVE_RIGHT = b'd'
KEY_MOVE_UP = b'w'
KEY_MOVE_DOWN = b's'
KEY_CENTER = b'c'
KEY_FRAME_FORWARD = b'.'
KEY_TOGGLE_HALTED = b'h'

THETA_3D_CSV = 'theta3d'
THETA_1D_CSV = 'theta1d'
OMEGA_3D_CSV = 'omega3d'
OMEGA_1D_CSV = 'omega1d'
ALPHA_3D_CSV = 'alpha3d'
ALPHA_1D_CSV = 'alpha1d'
TIME_CSV = 'time'

MASS_CSV = 'mass'
GRAVITY_CSV = 'gravity'
ARM_LENGTH_CSV = 'length'
ROOM_DIMENS_CSV = 'roomDimens'
BOB_DIMENS_CSV = 'bobDimens'
WINDOW_DIMENS_CSV = 'windowDimens'
CONNECTION_POINT_CSV = 'connectionPoint'
META_VARS_CSV = 'meta'
INIT_THETA_CSV = 'initTheta'
INIT_THETA_R_CSV = 'initThetaR'

PLOT_SIZE = (7.5, 6)  # Size is in inches for some weird reason
PLOT_TOP = 0.912
PLOT_BOTTOM = 0.121
PLOT_LEFT = 0.054
PLOT_RIGHT = 0.886
PLOT_W_SPACE = 0.25
#PLOT_H_SPACE = 0.197
PLOT_H_SPACE = 0.25
