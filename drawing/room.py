import numpy as np

import utils as Utils
import drawing.sat as Sat

from drawing.primatives.prism import Prism

class Room(Prism):
	_savedFloorPrism = 0
	_savedFloorData = ()
	def getFloor(self):
		if Utils.checkSavedData(self._savedFloorData, self._position, self._dimens, self._rotationD, self._center):
			return self._savedFloorPrism

		# Make a new prism with a very small height
		dimens = np.array([self._dimens[0], self._dimens[1], 1])
#		if self._center:
		pos = np.subtract(self.position, self._centerTrans)
#		pos = np.subtract(self.position, np.array([0, 0, self.dimens[2]]))

		self._savedFloorPrism = Prism(dimens, position = pos, rotation = self._rotationD, center = False, isRadians = False)
		self._savedFloorData = (self._position, self._dimens, self._rotationD, self._center)
		return self._savedFloorPrism
