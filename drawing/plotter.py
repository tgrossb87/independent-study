import numpy as np
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt

import constants as Consts

class SinglePlotter:
	def __init__(self, name = None, xLabel = "Time", lines = ['b-', 'r-', 'g-'], legend = [('blue', 'X'), ('red', 'Y'), ('green', 'Z')]):
		self.plotLines = None
		self.name = name if name is not None else ""
		self.xLabel = xLabel
		self.lines = lines
		self.legend = legend
		self.finished = False


	def appendData(self, data):
		rawDataT, rawDataYs = data
#		plt.legend()
		plt.grid()
		plt.draw()
		if isinstance(rawDataYs, (np.ndarray, list, tuple)):
#			print(rawDataYs)
#			dataYs = [[row[c] for row in rawDataYs] for c in range(len(rawDataYs))]
			dataYs = [rawDataY for rawDataY in rawDataYs]
			dataTs = [rawDataT for row in rawDataYs]
		else:
			dataYs = [rawDataYs]
			dataTs = [rawDataT]
		if self.plotLines is None:
			plt.ion()
			self.plotLines = []
			for c in range(len(dataYs)):
				line, = plt.plot(dataTs[c], dataYs[c], self.lines[c % len(self.lines)])
				self.plotLines.append(line)

			# Set up the legend the first time
#			left = plotIndex % self.shape[1] == 1
#			right = plotIndex % self.shape[1] == 0
#			labelBottom = plotIndex > (self.shape[0] - 1) * self.shape[1]
#			plt.tick_params(labelleft = False, labelright = True, labelbottom = labelBottom, left = False, right = True, bottom = True)
			plt.ylabel(self.name)

		else:
			for c in range(len(dataYs)):
				line = self.plotLines[c]
				line.set_xdata(np.append(line.get_xdata, dataTs[c]))
				line.set_ydata(np.append(line.get_ydata, dataYs[c]))

		legendHandles = []
#		rangeMin = min(len(self.legend), len(self.plotLines))
		for c in range(self.dimens):
			color, label = self.legend[c]
			legendHandles.append(mpatches.Patch(color = color, label = label))
		plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol = self.dimens, mode="expand", borderaxespad=0., handles = legendHandles)

#		plt.draw()
#		plt.show()

	def finish(self):
		self.finished = True


class SinglePlot:
	LEGEND, Y_LABEL, X_LABEL, Y_NUMS, X_NUMS = 0b00001, 0b00010, 0b00100, 0b01000, 0b10000
	def __init__(self, id, yLabel = " ", xLabel = "Time", dimens = 3, lines = ['b-', 'r-', 'g-'], legend = [("blue", "X"), ("red", "Y"), ("green", "Z")], toDraw = 0b00000):
		self.id = id
		self.yLabel = yLabel
		self.xLabel = xLabel
		self.dimens = dimens
		self.lines = lines if len(lines) <= dimens else lines[:dimens]
		self.legend = legend if len(legend) <= dimens else legend[:dimens]
		self.toDraw = toDraw

		# dimens+1 streams in the dataset
		# dataset[0] holds the times
		# All other indices hold y data for seperate lines
		self.dataset = [[] for c in range(self.dimens+1)]


	# dataT should be just a number
	# dataYs should be some sort of indexed collection with a length equal to dimens
	# dataYs can be a single number if dimens is 1
	def appendData(self, dataT, dataYs):
		# If dataYs is a list, break it down into seperate lines
		if isinstance(dataYs, (np.ndarray, list, tuple)):
			if not len(dataYs) == self.dimens:
				print("Recieved", dataYs, "(must be", self.dimens, "elements in length)");
				return False
			for c in range(self.dimens):
				self.dataset[c+1].append(dataYs[c])
		# If dataYs is a single value, just add it to the first y list
		else:
			self.dataset[1].append(dataYs)

		# Stick the time stamp to the first list in the dataset
		self.dataset[0].append(dataT)

		return True


	def shouldDraw(self, var):
		return self.toDraw & var


	def plot(self, shape, index):
		plt.subplot(*shape, index)
		for c in range(self.dimens):
			plt.plot(self.dataset[0], self.dataset[c+1], self.lines[c % len(self.lines)])

		if self.shouldDraw(SinglePlot.LEGEND):
			legendHandles = []
			for color, label in self.legend:
				legendHandles.append(mpatches.Patch(color = color, label = label))
			plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol = len(legendHandles), mode="expand", borderaxespad=0., handles = legendHandles)

		drawYNums = self.shouldDraw(SinglePlot.Y_NUMS)
		drawXNums = self.shouldDraw(SinglePlot.X_NUMS)
		plt.tick_params(labelleft = False, labelright = drawYNums, labelbottom = drawXNums, left = False, right = True, bottom = True)

		if self.shouldDraw(SinglePlot.Y_LABEL):
			plt.ylabel(self.yLabel)
		if self.shouldDraw(SinglePlot.X_LABEL):
			plt.xlabel(self.xLabel)


class MultiPlotter:
	def __init__(self, singlePlots, top = Consts.PLOT_TOP, bottom = Consts.PLOT_BOTTOM, left = Consts.PLOT_LEFT, right = Consts.PLOT_RIGHT,
					wspace = Consts.PLOT_W_SPACE, hspace = Consts.PLOT_H_SPACE, size = Consts.PLOT_SIZE):
		self.top = top
		self.bottom = bottom
		self.left = left
		self.right = right
		self.wspace = wspace
		self.hspace = hspace
		self.size = size

		self.shape = (len(singlePlots), len(singlePlots[0]))
		self.plots = [el for row in singlePlots for el in row]

		self.noNullPlotIndexMap = []
		self.plotIdsToIndices = {}
		for c in range(len(self.plots)):
			if self.plots[c] is not None:
				self.noNullPlotIndexMap.append(c)
				self.plotIdsToIndices[self.plots[c].id] = c
		self.plotIds = set(self.plotIdsToIndices.keys())


		self.finished = False


	def appendDataTo(self, dataT, dataYs, plot, ignoreNulls = False):
		if isinstance(plot, tuple):
			index = plot[0] * self.shape[0] + plot[1]
		else:
			index = plot

		if not ignoreNulls:
			if index >= len(self.plots) or index < 0:
				print("Error: Invalid index", plot, "(only", len(self.plots), "plots available")
				return False

			if self.plots[index] is None:
				print("Error: Attempted to add to a null plot")
				return False
		else:
			if index >= len(self.noNullPlotIndexMap) or index < 0:
				print("Error: Invalid index", plot, "(only", len(self.noNullPlots), "non-null plots available)")
				return False
			index = self.noNullPlotIndexMap[index]

		self.plots[index].appendData(dataT, dataYs)
		return True


	def appendDataById(self, dataT, dataYs, id):
		if id not in self.plotIdsToIndices:
			print("Error: Invalid id '" + str(id) + "' (acceptable ids:", str(self.plotIdsToIndices.keys()) + ")")
			return False

		self.plots[self.plotIdsToIndices[id]].appendData(dataT, dataYs)
		return True


	def finish(self):
		if self.finished:
			return False

		self.finished = True

		plt.figure(1, figsize = self.size)
		plt.subplots_adjust(top = self.top, bottom = self.bottom, left = self.left, right = self.right, wspace = self.wspace, hspace = self.hspace)

		for c in range(len(self.plots)):
			if self.plots[c] is None:
				continue
			self.plots[c].plot(self.shape, c+1)

		plt.show()


class Plotter:
	def __init__(self, plotCount, shape = None, names = None, xLabel = "Time", lines = ['b-', 'r-', 'g-'], legend = [('blue', 'X'), ('red', 'Y'), ('green', 'Z')], verticalData = False):
		self.plotCount = plotCount
		self.shape = shape if shape is not None else (self.plotCount, 1)
		self.names = names if names is not None else []
		self.xLabel = xLabel
		self.lines = lines if len(lines) > 0 else ['b-']
		self.legend = legend
		self.datasets = []
		for c in range(self.plotCount):
			self.datasets.append([[], []])
		self.verticalData = verticalData
		self.finished = False


	def appendData(self, *dataVals):
		if not len(dataVals) == self.plotCount:
			print("Supplied", len(dataVals), "not", self.plotCount, "values")
			return False
		for dataC in range(self.plotCount):
			self.appendDataTo(dataVals[dataC], dataC)
		return True


	def appendDataTo(self, dataVal, plot):
		if plot >= self.plotCount:
			print("Supplied", plot, "as a plot value, which is greater than or equal to", self.plotCount)
			return False
		dataT, dataY = dataVal
		self.datasets[plot][0].append(dataT)
		self.datasets[plot][1].append(dataY)
#		self.updateData(dataVal, self.plots[plot])


	def updateData(self, newData, plot):
		dataTs, dataYs = newData
		if isinstance(dataYs[0], (np.ndarray, list, tuple)):
			# Split the y data by column
			colYData = [[row[c] for row in dataYs] for c in range(len(dataYs[0]))]
			for c in range(len(colYData)):
				lineIndex = c % len(self.lines)
				plot[c].set_xdata(numpy.append(plot[c].get_xdata, dataTs))
				plot[c].set_ydata(numpy.append(plot[c].get_ydata, colYData[c]))
		else:
			plot.set_xdata(numpy.append(plot.get_xdata(), dataTs))
			plot.set_ydata(numpy.append(plot.get_ydata(), dataYs))
		plt.draw()


	def finish(self, ignoreAlreadyFinished = False):
		if self.finished and not ignoreAlreadyFinished:
			return

		self.finished = True

		plt.figure(1)

		# Increase the horizontal padding
		plt.subplots_adjust(wspace = 0.5)

		for datasetC in range(self.plotCount):
			if self.verticalData:
				# Convert the index number to a (row, col) representation vertically
				pltRows, pltCols = self.shape
				row, col = datasetC % pltRows, int(datasetC / pltRows)

				# Now convert the (row, col) representation back to a horizontal index
				plotIndex = row * pltCols + col + 1
			else:
				plotIndex = datasetC + 1
			if len(self.datasets[datasetC][0]) == 0:
				continue
			plt.subplot(*self.shape, plotIndex)
			dataTs, dataYs = self.datasets[datasetC]
			if isinstance(dataYs[0], (np.ndarray, list, tuple)):
				# Split the y data by column
				colYData = [[row[c] for row in dataYs] for c in range(len(dataYs[0]))]
				for c in range(len(colYData)):
					lineIndex = c % len(self.lines)
					plt.plot(dataTs, colYData[c], self.lines[lineIndex])
			else:
				plt.plot(dataTs, dataYs, self.lines[0])
			left = plotIndex % self.shape[1] == 1
			right = plotIndex % self.shape[1] == 0
			labelBottom = plotIndex > (self.shape[0] - 1) * self.shape[1]
			plt.tick_params(labelleft = False, labelright = True, labelbottom = labelBottom, left = False, right = True, bottom = True)
			plt.ylabel(self.names[datasetC] if datasetC < len(self.names) else "")

			if datasetC == 0:
				# Put the legend above the first plot
				legendHandles = []
				for color, label in self.legend:
					legendHandles.append(mpatches.Patch(color = color, label = label))
				plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol = len(legendHandles), mode="expand", borderaxespad=0., handles = legendHandles)

#			if plotIndex > (self.shape[0]-1) * self.shape[1]:
#				# Label bottom x-axis with time
#				plt.xlabel(self.xLabel)
#			else:
#				# Remove bottom ticks from all but the bottom plots
#				plt.xticks([])
		plt.show()


class ContinuousPlotter:
	fig, ax, line, plotCount = 0, 0, 0, 0
	def __init__(self, *args):
		pass

	def appendData(self, data, *args):
		if self.plotCount == 0:
			plt.ion()
			self.fig = plt.figure()
			self.ax = self.fig.add_subplot(111)
			self.line,  = self.ax.plot(0, data, 'r-')
#			plt.show(block=False)
#			plt.pause(0.0001)
		else:
			self.line.set_ydata(data)
			self.fig.canvas.draw()
			self.fig.canvas.flush_events()

#			self.ax.draw_artist(self.ax.patch)
#			self.ax.draw_artist(self.line)
#			self.fig.canvas.update()
#			self.fig.canvas.flush_events()
		self.plotCount += 1

	def finish(self):
		pass
#		plt.show()


#class MultiPlotter:
#	def __init__(self, plotCount):
#		self.plotCounter = [0 for c in range(plotCount)]
#		self.plots = [0 for c in range(plotCount)]
#		self.fig = plt.subplots()
#
#
#	def plotInitial(self, data, plot):
#		fig, ax = plt.subplots()
#		line, = ax.plot(data)
#		plt.show(block=False)
#		plt.pause(0.0001)
#		self.plots[plot] = (fig, ax, line)
#
#
#	def appendListData(self, newData, plot):
#		for data in newData:
#			self.appendData(data, plot)
#
#
#	def appendData(self, data, plot):
#		if self.plotCounter[plot] == 0:
#			self.plotInitial(data, plot)
#		else:
#			fig, ax, line = self.plots[plot]
#			line.set_ydata(data)
#			ax.draw_artist(ax.patch)
#			ax.draw_artist(line)
#			fig.canvas.update()
#			fig.canvas.flush_events()
#		self.plotCounter[plot] += 1
##		print("Appened data")
