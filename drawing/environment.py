import math

from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

import constants as Consts
from drawing.drawable import Drawable
from drawing.physicalBody import PhysicalBody

class Environment:
	time = 0

	# Callbacks
	oneTimePostLoop = None
	preStep = None

	window = 0
	zoom = Consts.INITIAL_ZOOM

	hX = Consts.INITIAL_HX
	hZ = Consts.INITIAL_HZ

	xPan = 0
	yPan = 0

	room = 0
	bodies = []
	objects = []

	def __init__(self, name, size, pos = (800, 200), drawFunction = None, stepFunction = None, startLoop = False, draw = True,
				axesColors = [(255, 0, 0), (0, 0, 255), (0, 255, 0)], axesLength = 50, axesPos = None):
		self.drawFunction = drawFunction if drawFunction is not None else lambda: 0
		self.stepFunction = stepFunction if stepFunction is not None else lambda: 0
		self.size = size
		self.draw = draw
		self.axesColors = axesColors
		self.axesLength = axesLength
		self.axesPos = axesPos if axesPos is not None else Consts.ROOM_DIMENS * -0.5
		self.halted = False
		self.singleStepFlag = False

		if self.draw:
			glutInit(sys.argv)
			glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH)
			glutInitWindowSize(*self.size)
			glutInitWindowPosition(*pos)

			window = glutCreateWindow(name)

			glutDisplayFunc(self.loop)
			glutIdleFunc(self.loop)
			glutKeyboardFunc(self.keyPressed)
			glutSpecialFunc(self.keySpecial)

			self.initGL(*self.size)

		if startLoop:
			self.activate()


	# Provide an accessory method to starting the main loop
	def activate(self):
		if self.draw:
			glutMainLoop()
		else:
			while not self.halted:
				self.loop()


	# Registers a function to be called once after the next draw loop is completed
	def addOneTimePostLoop(self, function):
		self.oneTimePostLoop = function


	# Registers a function to be called every time before a step happens, as long as a step would happen
	def addPreStep(self, function):
		self.preStep = function


	# Registers a body with the environment so body motion can be done automatically
	def registerBody(self, body):
		if not isinstance(body, PhysicalBody):
			print("registerBody received non-body")
			return False
		body.stepInDraw = False
		self.bodies.append(body)
		return True


	# Registers a static object with the environment so its drawing can be done automatically
	def registerObject(self, object):
		if not isinstance(object, Drawable):
			print("registerObject receieved non-drawable")
			return False
		self.objects.append(object)
		return True

	# Registers the room with the environment so it is drawn automatically and a refrence is kept elsewhere
	def registerRoom(self, room):
		if not isinstance(room, Drawable):
			print("registerRoom recieved non-drawable")
			return False
		self.room = room
		return True

	# Starts out all of the complicated openGL-ness
	# The important thing here is the perspective definition, which can be problematic with clipping
	def initGL(self, width, height):
		glClearColor(1, 1, 1, 1)
		glClearDepth(1.0)
		glDepthFunc(GL_LESS)
		glEnable(GL_DEPTH_TEST)
		glShadeModel(GL_SMOOTH)
		glMatrixMode(GL_PROJECTION)
		glLoadIdentity()
		gluPerspective(45.0, float(width) / float(height), 0.1, 3 * width)
		glMatrixMode(GL_MODELVIEW)


	def loop(self):
#		if self.preActiveLoop is not None and not (self.halted and not self.singleStepFlag):
#			self.preActiveLoop(self.time)

		if self.draw:
			stepped = self.drawAndStep()
		else:
			stepped = self.step()

		if self.oneTimePostLoop is not None:
			self.oneTimePostLoop(self.time)
			self.oneTimePostLoop = None

		if stepped:
			self.time += 1


	# This handles the physical motion
	# Loop and draw can call step without fear, it will handle
	# the whole halted and flag thing
	# Return: If the step was completed or not (for timing purposes)
	def step(self):
		if self.halted and not self.singleStepFlag:
			return False

		if self.preStep is not None:
			self.preStep(self.time)

		printData = False
		if self.halted and self.singleStepFlag:
			printData = True
			self.singleStepFlag = False

		if not len(self.bodies) == 0:
			for body in self.bodies:
				data = body.step()
				if printData:
					print("Time:", self.time, "  ", data)

		self.stepFunction()

		return True


	# The main draw loop
	# Return: If a step was completed or not (for timing purposes)
	def drawAndStep(self):
		stepped = self.step()

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

		glLoadIdentity()

		glTranslatef(self.xPan, self.yPan, self.zoom)
		glRotatef(self.hX, 1.0, 0.0, 0.0)
		glRotatef(self.hZ, 0.0, 0.0, 1.0)

		glViewport(0, 0, *self.size)

		# Draw axes
		if self.axesLength > 0:
			glTranslatef(*self.axesPos)
			glLineWidth(5.0)
			glBegin(GL_LINES)
			for c in range(len(self.axesColors)):
				glColor(*self.axesColors[c])
				glVertex3f(0, 0, 0)
				glVertex3f(self.axesLength if c == 0 else 0, self.axesLength if c == 1 else 0,  self.axesLength if c == 2 else 0)
			glEnd()
			glTranslatef(*(-1 * self.axesPos))

		# Activate all bodies and draw all objects and the room before yielding control to the parent draw function
		if isinstance(self.room, Drawable):
			self.room.draw()

		if not len(self.bodies) == 0:
			for body in self.bodies:
				body.draw()

		if not len(self.objects) == 0:
			for object in self.objects:
				object.draw()

		# Call the external draw function
		self.drawFunction()

		glutSwapBuffers()

		return stepped


	#################################################
	#                                               #
	#   These two functions handle keyboard input   #
	#                                               #
	#################################################

	def keyPressed(self, *args):
		if args[0] == '\033':
			 sys.exit()

		# Set the single step flag to true if the environment is halted and advanced
		elif args[0] == Consts.KEY_FRAME_FORWARD:
			if self.halted:
				self.singleStepFlag = True
				return

		# Toggle halted
		elif args[0] == Consts.KEY_TOGGLE_HALTED:
			self.halted = not self.halted
			if not self.halted:
				self.singleStepFlag = False
				return

		 # Snap all modifiers to initial
		elif args[0] == Consts.KEY_SNAP_BACK:
			self.hX = Consts.INITIAL_HX
			self.hZ = Consts.INITIAL_HZ
			self.zoom = Consts.INITIAL_ZOOM
			self.xPan = 0
			self.yPan = 0

		# Zoom in
		elif args[0] == Consts.KEY_ZOOM_IN:
			self.zoom += Consts.ZOOM_SPEED

		# Zoom out
		elif args[0] == Consts.KEY_ZOOM_OUT:
			self.zoom -= Consts.ZOOM_SPEED

		# Moves the model
		elif args[0] == Consts.KEY_MOVE_LEFT:
			self.xPan -= Consts.PAN_SPEED
		elif args[0] == Consts.KEY_MOVE_UP:
			self.yPan += Consts.PAN_SPEED
		elif args[0] == Consts.KEY_MOVE_RIGHT:
			self.xPan += Consts.PAN_SPEED
		elif args[0] == Consts.KEY_MOVE_DOWN:
			self.yPan -= Consts.PAN_SPEED

		# Centers the model on the screen
		elif args[0] == Consts.KEY_CENTER:
			self.xPan = 0
			self.yPan = 0
		self.debug()


	def keySpecial(self, *args):
		# Arrow keys rotate the model
		# If Consts.MODEL_PERSPECTIVE is enabled, the model's spin
		# direction is relative to the model, inverted from
		# the user's perspective.
		step = -1 * Consts.ROTATE_SPEED if Consts.MODEL_PERSPECTIVE else 1 * Consts.ROTATE_SPEED

		if args[0] == GLUT_KEY_LEFT:
			self.hZ += step
		elif args[0] == GLUT_KEY_RIGHT:
			self.hZ -= step
		elif args[0] == GLUT_KEY_UP:
			self.hX += step
		elif args[0] == GLUT_KEY_DOWN:
			self.hX -= step

		self.debug()


	def debug(self):
		print("Updated PAN: (%f, %f)  ZOOM: %f  ROTATION: (%f, 0, %f)" % (self.xPan, self.yPan, self.zoom, self.hX, self.hZ))
