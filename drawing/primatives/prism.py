import utils as Utils
import constants as Consts
import drawing.sat as Sat
from drawing.drawable import Drawable

from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
import math
import numpy as np

class Prism(Drawable):
	# All have an extra 0 at the end to that the only things with a bit length of 1 are NONE and ALL
	TOP, FRONT, RIGHT, BACK, LEFT, BOTTOM = 0b0000010, 0b0000100, 0b0001000, 0b0010000, 0b0100000, 0b1000000
	X, Y, Z = 0b0010, 0b0100, 0b1000
	NORMALS, VERTICES, EDGES, DEF_POINT = 0b10, 0b100, 0b1000, 0b10000
	NONE, ALL = 0b0, 0b1

	FACES = [[0, 1, 2, 3],
		[1, 5, 6, 2],
		[1, 0, 4, 5],
		[0, 3, 7, 4],
		[2, 6, 7, 3],
		[4, 5, 6, 7]
	]

	WIREFRAME_EDGE_ORDER = [
		(0, 1), (0, 3), (1, 2), (2, 3),  # Top
		(4, 5), (4, 7), (5, 6), (6, 7),  # Bottom
		(0, 4), (1, 5), (2, 6), (3, 7)   # Connecting edges
	]

	VERTEX_MAP = [
		[1.0, 1.0, 1.0],
		[1.0, 0.0, 1.0],
		[0.0, 0.0, 1.0],
		[0.0, 1.0, 1.0],
		[1.0, 1.0, 0.0],
		[1.0, 0.0, 0.0],
		[0.0, 0.0, 0.0],
		[0.0, 1.0, 0.0]
	]

	def __init__(self, dimens, position = Utils.zeros3d(), rotation = Utils.zeros3d(), isRadians = True,
				faceColor = (255, 0, 0), vertexColor = (0, 0, 255), normalColor = (0, 0, 255), edgeColor = (0, 0, 0), defPointColor = (255, 0, 0),
				center = 0b1, faces = 0b0, shouldDraw = 0b1, permTrans = Utils.zeros3d()):
		self.setDimens(dimens, recalculateCenterTrans = False)
		self.setPosition(position)
		self.setRotation(rotation, isRadians)
		self.setCenter(center)
		self.setFaces(faces)
		self.setShouldDraw(shouldDraw)
		self.setPermTrans(permTrans)
		self.setColors(faceColor, vertexColor, normalColor, edgeColor, defPointColor)
		self.getShapeDefinition()


	def draw(self):
		x, y, z = self._dimens
		vertices, normals = self.getShapeDefinition()

		# Draw the cube using the verticed of the shape definition
		glBegin(GL_QUADS)
		glColor3f(*self._faceColor)
		for faceIndex in range(len(self.FACES)):
			if not Utils.bitSetAt(self._faces, faceIndex+1):
				continue
			for vertex in self.FACES[faceIndex]:
				glVertex3f(*vertices[vertex])
		glEnd()

		self.drawShapeDefinitions()


	# Draw the shape definition absolutely
	def drawShapeDefinitions(self):
		x, y, z = self._position
		w, d, h = self._dimens
		smallestDimen = min(w, min(d, h))
		vertices, normals = self.getShapeDefinition()

		glPushMatrix()

		if Utils.bitVarSet(self._shouldDraw, Prism.VERTICES):
			glPointSize(5.0)
			glBegin(GL_POINTS)
			glColor(*self._vertexColor)
			for vertex in vertices:
				glVertex3f(*vertex)
			glEnd()

		if Utils.bitVarSet(self._shouldDraw, Prism.EDGES):
			glLineWidth(2.0)
			glBegin(GL_LINES)
			glColor(*self._edgeColor)
			# Loop through the vertices and draw a line between the next two vertices
			for p1, p2 in self.WIREFRAME_EDGE_ORDER:
				glVertex3f(*vertices[p1])
				glVertex3f(*vertices[p2])
			glEnd()

		if Utils.bitVarSet(self._shouldDraw, Prism.NORMALS):
			glBegin(GL_LINES)
			glColor(*self._normalColor)
			# Loop through the vertices and draw the next normal between the current face vertices
			for c in range(len(self.FACES)):
				points = []
				for point in self.FACES[c]:
					points.append(vertices[point])
				center = np.array(points).mean(axis = 0)

				glVertex3f(*center)
				glVertex3f(*np.add(center, normals[c]))

			glEnd()

		if Utils.bitVarSet(self._shouldDraw, Prism.DEF_POINT):
			defPoint = np.add(self._position, self._dimens/2)
			np.add(defPoint, self._centerTrans, defPoint)
			glBegin(GL_POINTS)
			glColor(*self._defPointColor)
			glVertex3f(*self._position)
			glEnd()

		glPopMatrix()


	# Caches the bounding box and only recalculates when position, dimens, or rotation changes
	_savedShapeData = ()
	_savedShapeDef = ()
	def getShapeDefinition(self):
		# Check if the saved data can be used still
		if Utils.checkSavedData(self._savedShapeData, self._position, self._dimens, self._rotationD, self._center):
			return self._savedShapeDef

		box = []
		w, d, h = self._dimens
		for wm, dm, hm in self.VERTEX_MAP:
			rawPoint = np.array([w * wm, d * dm, h * hm])

			# Move the raw point up to the proper position
			np.add(rawPoint, self._position, rawPoint)

			# Move the raw point so it is centered properly
			np.add(rawPoint, self._centerTrans, rawPoint)

			# Move the raw point by the perminant translation vector
			np.add(rawPoint, self._permTrans, rawPoint)

			# Rotate the point about the positioned center
			rotPoint = Utils.rotatePointAbout(rawPoint, self._position, self._trigs)

			box.append(rotPoint)
		box = np.array(box)

		minDimen = min(w, min(d, h))
		normals = Sat.getNormals(self.FACES, box, flippedIndices = [5], normalizedLength = minDimen)
		self._savedShapeDef = (box, normals)
		self._savedShapeData = (self._position, self._dimens, self._rotationD, self._center)

		return self._savedShapeDef


	def getDimens(self):
		return self._dimens

	def setDimens(self, newDimens, recalculateCenterTrans = True):
		ok = Utils.check3dList(newDimens, "newDimens", "setDimens", "Prism")
		if ok:
			self._dimens = newDimens.copy()

			# Changing the dimensions requires the center translation list to be changed, so do that
			if recalculateCenterTrans:
				self._calculateCenterTrans()


	def getPosition(self):
		return self._position


	def setPosition(self, newPosition):
		ok = Utils.check3dList(newPosition, "newPosition", "setPosition", "Prism")
		if ok:
			self._position = newPosition.copy()


	def getPermTrans(self):
		return self._permTrans


	def setPermTrans(self, newPermTrans):
		ok = Utils.check3dList(newPermTrans, "newPermTrans", "setPermTrans", "Prism")
		if ok:
			self._permTrans = newPermTrans.copy()


	def getColors(self):
		return (self._faceColor, self._vertexColor, self._normalColor, self._edgeColor)


	def setColors(self, faceColor = None, vertexColor = None, normalColor = None, edgeColor = None, defPointColor = None):
		if faceColor is not None:
			ok = Utils.checkColorList(faceColor,  "faceColor", "setColor", "Prism")
			if ok:
				self._faceColor = faceColor
		if vertexColor is not None:
			ok = Utils.checkColorList(vertexColor, "vertexColor", "setColor", "Prism")
			if ok:
				self._vertexColor = vertexColor
		if normalColor is not None:
			ok = Utils.checkColorList(normalColor, "normalColor", "setColor", "Prism")
			if ok:
				self._normalColor = normalColor
		if edgeColor is not None:
			ok = Utils.checkColorList(edgeColor, "edgeColor", "setColor", "Prism")
			if ok:
				self._edgeColor = edgeColor
		if defPointColor is not None:
			ok = Utils.checkColorList(defPointColor,  "defPointColor", "setColor", "Prism")
			if ok:
				self._defPointColor = defPointColor


	def getRotation(self):
		return (self._rotationD, self._rotationR)


	def getTrigs(self):
		return self._trigs


	def setRotation(self, newRotation, isRadians = False):
		ok = Utils.check3dList(newRotation, "newRotation", "setRotation", "Prism")
		if ok:
			if isRadians:
				self._rotationR = newRotation.copy()
				self._rotationD = np.array([math.degrees(ax) for ax in self._rotationR])
			else:
				self._rotationD = newRotation.copy()
				self._rotationR = np.array([math.radians(ax) for ax in self._rotationD])
			self._trigs = [{Consts.COS: math.cos(ax), Consts.SIN: math.sin(ax)} for ax in self._rotationR]


	def getCenter(self):
		return self._center


	def setCenter(self, center):
		if isinstance(center, bool):
			self._center = Utils.bitwiseFromBool(center, 3) << 1
			self._calculateCenterTrans()
		else:
			ok = Utils.checkBitwise(center, 4, "center", "setCenter", "Prism")

			# If it has a bit length of 1 or 0, it is either ALL or NONE
			if ok and center.bit_length() < 2:
				self._center = Utils.bitwiseFromBool(center == Prism.ALL, 3) << 1
			elif ok:
				self._center = center

			if ok:
				self._calculateCenterTrans()


	def getCenterTrans(self):
		return self._centerTrans


	def _calculateCenterTrans(self):
		self._centerTrans = [0.0, 0.0, 0.0]
		for c in range(3):
			if Utils.bitSetAt(self._center, c+1):
				self._centerTrans[c] = -self._dimens[c] / 2.0
		self._centerTrans = np.array(self._centerTrans)



	def getFaces(self):
		return self._faces


	def setFaces(self, faces):
		if isinstance(faces, bool):
			self._faces = Utils.bitwiseFromBool(faces, 6) << 1
		else:
			ok = Utils.checkBitwise(faces, 7, "faces", "setFaces", "Prism")

			# If it has a bit length of 1 or 0, it is either ALL or NONE
			if ok and faces.bit_length() < 2:
				self._faces = Utils.bitwiseFromBool(faces == Prism.ALL, 6) << 1
			elif ok:
				self._faces = faces


	def getShouldDraw(self):
		return self._shouldDraw


	def setShouldDraw(self, shouldDraw):
		if isinstance(shouldDraw, bool):
			self._shouldDraw = Utils.bitwiseFromBool(shouldDraw, 4) << 1
		else:
			ok = Utils.checkBitwise(shouldDraw, 5, "shouldDraw", "setShouldDraw", "Prism")

			# If it has a bit length of 1 or 0, it is either ALL or NONE
			if ok and shouldDraw.bit_length() < 2:
				self._shouldDraw = Utils.bitwiseFromBool(shouldDraw == Prism.ALL, 4) << 1
			elif ok:
				self._shouldDraw = shouldDraw
