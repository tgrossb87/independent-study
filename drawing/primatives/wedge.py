import utils as Utils
import sat as Sat
from drawable import Drawable

from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
import math
import numpy as np

class Wedge(Drawable):
	FACES = [[0, 1, 2],    # Front
		[0, 1, 4, 3],  # Bottom
		[1, 4, 5, 2],  # Top
		[0, 2, 5, 3],  # Left
		[3, 4, 5]      # Back
	]

	WIREFRAME_EDGE_ORDER = [
		(0, 1), (0, 2), (1, 2),  # Back
		(3, 4), (3, 5), (4, 5),  # Front
		(0, 3), (1, 4), (2, 5)   # Connecting edges
	]

	VERTEX_MAP = [
		[0.0, 0.0, 0.0],
		[1.0, 0.0, 0.0],
		[0.0, 0.0, 1.0],
		[0.0, 1.0, 0.0],
		[1.0, 1.0, 0.0],
		[0.0, 1.0, 1.0]
	]

	def __init__(self, length, angle, width, color = (0, 255, 0), edgeColor = (0, 0, 0), vertexColor = (0, 0, 255), normalColor = (0, 0, 255),
				position = Utils.zeros3d(), rotation = Utils.zeros3d(), isRadians = True, yCenter = True, drawBoundingVertices = True, drawNormals = True):
		self.length = length
		self.angle = angle if isRadians else math.radians(angle)
		self.width = width
		self.color = color
		self.edgeColor = edgeColor
		self.vertexColor = vertexColor
		self.normalColor = normalColor
		self.position = position
		self.rotation = rotation if not isRadians else [math.degrees(theta) for theta in rotation]
		self.yCenter = yCenter
		self.drawBoundingVertices = drawBoundingVertices
		self.drawNormals = drawNormals


	def draw(self):
		x, z = self.calcWidthAndHeight()

		glPushMatrix()

		glTranslatef(*self.position)
		if self.yCenter:
			glTranslatef(0.0, -self.width/2, 0.0)

		glRotatef(self.rotation[0], 1.0, 0.0, 0.0)
		glRotatef(self.rotation[1], 0.0, 1.0, 0.0)
		glRotatef(self.rotation[2], 0.0, 0.0, 1.0)

		glBegin(GL_POLYGON)
		glColor(*self.color)
		for face in self.FACES:
			for vertex in face:
				xm, wm, zm = self.VERTEX_MAP[vertex]
				glVertex3f(x * xm, self.width * wm, z * zm)
		glEnd()

		glLineWidth(2.0)
		glBegin(GL_LINES)
		glColor(*self.edgeColor)
		for edge in self.WIREFRAME_EDGE_ORDER:
			for vertex in edge:
				xm, wm, zm = self.VERTEX_MAP[vertex]
				glVertex3f(x * xm, self.width * wm, z * zm)
		glEnd()

		glPopMatrix()
		self.drawShapeDefinition()


	# Cached values to avoid repetative (and expensive) trigonometric calls
	savedWidthAndHeight = ()
	savedLengthAndAngle = ()
	def calcWidthAndHeight(self):
		# Check if the last calculated value used the same angle and length as we have now
		# If so, return the saved tuple
		if self.savedLengthAndAngle == (self.length, self.angle):
			return self.savedWidthAndHeight

		self.savedLengthAndAngle = (self.length, self.angle)
		self.savedWidthAndHeight = (self.length * math.cos(self.angle), self.length * math.sin(self.angle))
		return self.savedWidthAndHeight


	# An accessory method to calculate the position after traveling dx units down the wedge
	def calcPositionAt(self, dx):
		width, height = self.calcWidthAndHeight()

		relativeX = (self.length - dx) * math.cos(self.angle)
		# RelativeX is relative to the right most point, position is of the left most
		absX = self.position[0] + width - relativeX

		relativeZ = (self.length - dx) * math.sin(self.angle)
		absZ = self.position[2] + relativeZ

		absY = self.position[1] - self.width/2 if self.yCenter else self.position[1]
		return (absX, absY, absZ)


	# Draw the shape definition absolutely
	def drawShapeDefinition(self):
		x, y, z = self.position
		vertices, normals = self.getShapeDefinition()

		glPushMatrix()

		if self.drawBoundingVertices:
			glPointSize(5.0)
			glBegin(GL_POINTS)
			glColor(*self.vertexColor)
			for vertex in vertices:
				glVertex3f(*vertex)
			glEnd()

		if self.drawNormals:
			glBegin(GL_LINES)
			glColor(*self.normalColor)
			# Loop through the vertices and draw the next normal between the next two vertices
			for c in range(len(self.FACES)):
				points = []
				for point in self.FACES[c]:
					points.append(vertices[point])
				center = np.array(points).mean(axis = 0)

				glVertex3f(*center)
				glVertex3f(*np.add(center, normals[c]))

			glEnd()

		glPopMatrix()


	# Caches the bounding box and only recalculates when position, dimens, or rotation changes
	savedShapeData = ()
	savedShapeDef = ()
	def getShapeDefinition(self):
		if Utils.checkSavedData(self.savedShapeData, self.position, self.length, self.angle, self.width, self.rotation, self.yCenter):
			return self.savedShapeDef

		w, h = self.calcWidthAndHeight()
		x, y, z = self.position

		box = []
		for wm, dm, hm in self.VERTEX_MAP:
			box.append([x + w * wm, y + self.width * dm, z + h * hm])
		box = np.array(box)

		if self.yCenter:
			box -= np.array([0, self.width/2, 0])

		rotationPoint = self.position if not self.yCenter else np.add(self.position, np.array([0, self.width/2, 0]))

		trigs = []
		for theta in self.rotation:
			trigs.append([math.sin(theta), math.cos(theta)])

		box = np.array([self.rotate(point, trigs, rotationPoint) for point in box])

		minDimen = min(min(w, h), min(self.width, self.length))
		normals = Sat.getNormals(self.FACES, box, flippedIndices = [0, 2, 3], normalizedLength = minDimen)
		self.savedShapeDef = (box, normals)
		self.savedShapeData = (self.position, self.length, self.angle, self.width, self.rotation, self.yCenter)

		return self.savedShapeDef


	def rotate(self, point, trigs, about):
		origPoint = np.subtract(point, about).reshape((3, 1))

		xRotMatrix = np.array([
			[1, 0, 0],
			[0, trigs[0][1], -1*trigs[0][0]],
			[0, trigs[0][0], trigs[0][1]]
		])

		yRotMatrix = np.array([
			[trigs[1][1], 0, trigs[1][0]],
			[0, 1, 0],
			[-1 * trigs[1][0], 0, trigs[1][1]]
		])

		zRotMatrix = np.array([
			[trigs[2][1], -1 * trigs[2][0], 0],
			[trigs[2][0], trigs[2][1], 0],
			[0, 0, 1]
		])

		prime = np.matmul(xRotMatrix, origPoint)
		doublePrime = np.matmul(yRotMatrix, prime)
		triplePrime = np.matmul(zRotMatrix, doublePrime)

		return np.add(triplePrime.reshape(3), about)
