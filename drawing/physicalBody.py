import numpy as np
import math as math

from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *

import constants as Consts
import utils as Utils
import drawing.sat as Sat

from drawing.room import Room

class PhysicalBody:
	def __init__():
		pass


class Pendulum(PhysicalBody):
	def __init__(self, object, mass, armLength, connectionPoint = Consts.CONNECTION_POINT,
				omega = Utils.zeros3d(), gravity = Consts.GRAVITY, omegaColor = (255, 0, 0), stepInDraw = True):
		self.object = object
		self.mass = mass
		self.armLength = armLength
		self.connectionPoint = connectionPoint
		self.omega = omega.copy()
		self.gravity = np.array([gravity, 0, 0])
		self.alpha = self.gravity * self.object.getTrigs()[0][Consts.SIN]
		self.omegaColor = omegaColor
		self.stepInDraw = stepInDraw

		self.theta = self.object.getRotation()[0]

		self.halted = False


	def halt(self):
		self.halted = True


	def step(self):
		if self.halted:
			return ()

		self.alpha = -self.gravity * self.object.getTrigs()[0][Consts.SIN] * self.mass
		np.add(self.omega, self.alpha, self.omega)

		np.add(self.theta, self.omega, self.theta)
		self.object.setRotation(self.theta, isRadians = False)

		trigs = self.object.getTrigs()

		cartPos = Utils.polarToCartesian(self.armLength, trigs)
		np.subtract(self.connectionPoint, cartPos, cartPos)

		self.object.setPosition(cartPos)
		return (self.theta, self.omega, self.alpha)


	def draw(self):
		if self.stepInDraw:
			self.step()
		self.object.draw()

		pos = self.object.getPosition()
		objCenter = np.subtract(pos, self.object.getCenterTrans())
		np.subtract(objCenter, self.object.getDimens()/2, objCenter)
		np.subtract(objCenter, self.object.getPermTrans())
		objCenter = Utils.rotatePointAbout(objCenter, pos, self.object.getTrigs())

		perpTrigs = Utils.constructTrigsDef(np.subtract(self.object.getRotation()[1], [math.pi/2, 0, 0]))
		omegaVec = Utils.polarToCartesian(Utils.to1d(self.omega)*50, perpTrigs)

		glLineWidth(2.0)
		glBegin(GL_LINES)
		glColor(self.omegaColor)
		glVertex3f(*objCenter)
		glVertex3f(*np.add(objCenter, omegaVec))
		glEnd()


	@staticmethod
	def getValueIds(all = True):
		if all:
			return [Consts.THETA_3D_CSV, Consts.THETA_1D_CSV, Consts.OMEGA_3D_CSV, Consts.OMEGA_1D_CSV, Consts.ALPHA_3D_CSV, Consts.ALPHA_1D_CSV, Consts.MASS_CSV, Consts.ARM_LENGTH_CSV]
		else:
			return [Consts.THETA_3D_CSV, Consts.OMEGA_3D_CSV, Consts.ALPHA_3D_CSV]


	def getValues(self):
		thetaDisplacement = Utils.to1d(self.theta)
		omegaMag = Utils.to1d(self.omega)
		alphaMag = Utils.to1d(self.alpha)
		ids = Pendulum.getValueIds()
		return {
			ids[0]: self.theta, ids[1]: thetaDisplacement,
			ids[2]: self.omega, ids[3]: omegaMag,
			ids[4]: self.alpha, ids[5]: alphaMag,
			ids[6]: self.mass, ids[7]: self.armLength
		}
