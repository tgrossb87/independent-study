class Drawable:
	def draw(self):
		print("Drawable subclass does not override method 'draw' or calls super for no reason")

	def getShapeDefinition(self):
		print("Drawable subclass does not override method 'getShapeDefinition' or calls super for no reason")
