import numpy as np
import utils as Utils

from drawing.drawable import Drawable

def checkCollision(object1, object2):
	if not isinstance(object1, Drawable):
		print("checkCollision parameter 1 is not a drawable")
		return None
	if not isinstance(object2, Drawable):
		print("checkCollision parameter 2 is not a drawable")
		return None


	v1, n1 = object1.getShapeDefinition()
	v2, n2 = object2.getShapeDefinition()

	normals = np.concatenate([n1, n2])

	pushVecs = []
	for normal in normals:
		separates, pv = checkSeparatingAxis(normal, v1, v2)
		if separates:
			return False, None
		else:
			pushVecs.append(pv)

	# Pick the smallest push vector now (MPV)
	mpv = min(pushVecs, key = lambda v: np.dot(v, v))

	# Make sure mpv pushes the right way (push object 1 away, not farther in)
	dist = getCenterDist(v1, v2)

	# Invert mpv if it is in the same direction as the center displacement
	if np.dot(dist, mpv) > 0:
		mpv *= -1

	return True, mpv


def checkSeparatingAxis(norm, v1, v2):
	min1, max1 = getProjectionRange(norm, v1)
	min2, max2 = getProjectionRange(norm, v2)

	if max1 >= min2 and max2 >= min1:
		d = min(max2 - min1, max1 - min2)
		# Make the push vec a bit bigger so shapes don't overlap in the future
		# Needed because of float precision
		dPrime = d / np.dot(norm, norm) + 1e-10
		pv = dPrime * norm
		return False, pv

	return True, None


def getProjectionRange(norm, vertices):
	projMin, projMax = None, None
	for vertex in vertices:
		proj = np.dot(vertex, norm)

		projMin = proj if projMin is None else min(projMin, proj)
		projMax = proj if projMax is None else max(projMax, proj)
	return projMin, projMax

# Get the distance between the geographical centers of each set of points
def getCenterDist(v1, v2):
	c1 = np.mean(v1, axis=0)
	c2 = np.mean(v2, axis=0)
	return c2 - c1


# This method is here so others can use it
# Others can store their normals as part of their shape definitions, which allows
# for some optimizations
# Indexes that are flipped need to be defined
def getNormals(faces, vertexMap, flippedIndices, normalizedLength = None):
	normals = []
	for c in range(len(faces)):
		face = faces[c]
		if len(face) < 3:
			print("Error, face has less than 3 values")
			return None
		# Get the first three points of the face to define the plane
		p1 = np.array(vertexMap[face[0]])
		p2 = np.array(vertexMap[face[1]])
		p3 = np.array(vertexMap[face[2]])

		# Compute two vectors on the plane
		v1 = p3 - p1
		v2 = p2 - p1

		# Get the normal to the plane
		norm = np.cross(v1, v2)
		if normalizedLength is not None:
			norm = (norm / np.linalg.norm(norm)) * normalizedLength
		normals.append(norm if c not in flippedIndices else -1 * norm)
	return np.array(normals)
