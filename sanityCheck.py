#!/usr/bin/env python3

import argparse as ArgParser

import os
import sys
import csv

import numpy as np

import constants as Consts


def parseArgs():
	parser = ArgParser.ArgumentParser()
	parser.add_argument("-d", "--directory", default = "data", metavar = "directory", help = "The directory to sanity check.")
	parser.add_argument("-e", "--epsilon", default = 0.05, metavar = "epsilon", help = "The margin of error to allow.")
	return parser.parse_args()


def parseNpArray(npString):
	strVals = npString[1:-1].split()
	floatVals = [float(strVal) for strVal in strVals]
	return np.array(floatVals)


def readFileToDict(fileName, dir):
	outputDicts = []
	with open(os.path.join(dir, fileName), newline = '') as csvFile:
		reader = csv.DictReader(csvFile, delimiter = ',', quotechar = '"', quoting = csv.QUOTE_ALL)

		for row in reader:
			parsedDict = {}
			for varName, varVal in row.items():
				if varVal[0] == "[":
					parsedDict[varName] = parseNpArray(varVal)
				else:
					parsedDict[varName] = float(varVal)
			outputDicts.append(parsedDict)
	return outputDicts


def readFileStemToDicts(fileStem, dir):
	metaDict = readFileToDict(fileStem + "." + Consts.META_VARS_CSV + ".csv", dir)[0]
	valuesDicts = readFileToDict(fileStem + ".csv", dir)
	return (metaDict, valuesDicts)


def sanityCheck(dir, epsilon):
	fileStems = []
	for file in os.listdir(dir):
		if file.endswith("." + Consts.META_VARS_CSV + ".csv"):
			fileStems.append(file[:-1*len("." + Consts.META_VARS_CSV + ".csv")])


	for fileStem in fileStems:
		metaDict, valuesDicts = readFileStemToDicts(fileStem, dir)

		# Check that yt stays 0 (+- epsilon), zt stays the same (+- epsilon), and xt stays in -xi-epsilon <= xt <= xi+epsilon
		xi, yi, zi = metaDict[Consts.INIT_THETA_CSV]
		for valuesDict in valuesDicts:
			xt, yt, zt = valuesDict[Consts.THETA_3D_CSV]
			time = valuesDict[Consts.TIME_CSV]

			if xt > xi + epsilon or xt < -1 * xi - epsilon:
				print("Xt (xt:", xt, ", xi:", xi, ") error for time", time, "in simulation", fileStem)
				sys.exit()
			if yt < -1 * epsilon or yt > epsilon:
				print("Yt (yt:", yt, ") error for time", time, "in simulation", fileStem)
				sys.exit()
			if zt < zi - epsilon or zt > zi + epsilon:
				print("Zt (zt:", zt, ", zi:", zi, ") error for time", time, "in simulation", fileStem)
				sys.exit()
	print("Sanity checks passed")


def main():
	args = parseArgs()

	if not os.path.isdir(args.directory):
		print("Error: Directory (" + args.directory + ") does not exist or is not a directory")
		sys.exit()

	sanityCheck(args.directory, args.epsilon)


if __name__ == "__main__":
	main()
