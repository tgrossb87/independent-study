import numpy as np

from drawing.plotter import SinglePlot, MultiPlotter
from drawing.environment import Environment
from drawing.room import Room
from drawing.physicalBody import Pendulum
from drawing.primatives.prism import Prism

from OpenGL.GL import *

import constants as Consts
import utils as Utils


useCeil = 0b1111111 & 0b0
useYNums = 0b11111 & 0b0
environment = 0
bobBody = 0
multiPlotter = 0

def step():
	global multiPlotter

	if not multiPlotter.finished:
		bobVars = bobBody.getValues();
		for bobVarName, bobVarVal in bobVars.items():
			multiPlotter.appendDataById(environment.time, bobVarVal, bobVarName)
		multiPlotter.appendDataById(environment.time, Consts.GRAVITY, Consts.GRAVITY_CSV)

	if environment.time > Consts.SIM_LENGTH:
		bobBody.halt()
		environment.addOneTimePostLoop(finish)


def draw():
	# Could just draw to a translated object position, but this is a good sanity check
	ep = Utils.polarToCartesian(Consts.ARM_LENGTH, bobBody.object.getTrigs())
	np.subtract(Consts.CONNECTION_POINT, ep, ep)

	glLineWidth(2.0)
	glColor(255, 0, 0)
	glBegin(GL_LINES)
	glVertex3f(*Consts.CONNECTION_POINT)
	glVertex3f(*ep)
	glEnd()

	# Just for style points, draw nodes at the ends of the arm
	glBegin(GL_POINTS)
	glVertex3f(*Consts.CONNECTION_POINT)
	glVertex3f(*ep)
	glEnd()


def finish(t):
	global environment, multiPlotter
	environment.halted = True
	multiPlotter.finish()


def initPlotters():
	global multiPlotter

	ids = Pendulum.getValueIds()

	normedLines = ['c-']
	normedLegend = [("cyan", "Normalized")]

	constLines = ['m-']
	constLegend = [("magenta", "Constant")]

	theta3d = SinglePlot(id = ids[0], yLabel = "Theta", dimens = 3, toDraw = SinglePlot.LEGEND | SinglePlot.Y_LABEL | (SinglePlot.Y_NUMS & useYNums))
	theta1d = SinglePlot(id = ids[1], yLabel = "Theta", dimens = 1, toDraw = SinglePlot.LEGEND | SinglePlot.Y_LABEL | (SinglePlot.Y_NUMS & useYNums), lines = normedLines, legend = normedLegend)

	omega3d = SinglePlot(id = ids[2], yLabel = "Omega", dimens = 3, toDraw = SinglePlot.Y_LABEL | (SinglePlot.Y_NUMS & useYNums))
	omega1d = SinglePlot(id = ids[3], yLabel = "Omega", dimens = 1, toDraw = SinglePlot.Y_LABEL | (SinglePlot.Y_NUMS & useYNums), lines = normedLines)

	alpha3d = SinglePlot(id = ids[4], yLabel = "Alpha", dimens = 3, toDraw = SinglePlot.Y_LABEL | (SinglePlot.Y_NUMS & useYNums) | SinglePlot.X_LABEL | SinglePlot.X_NUMS)
	alpha1d = SinglePlot(id = ids[5], yLabel = "Alpha", dimens = 1, toDraw = SinglePlot.Y_LABEL | (SinglePlot.Y_NUMS & useYNums) | SinglePlot.X_LABEL | SinglePlot.X_NUMS, lines = normedLines)

	gravity = SinglePlot(id = Consts.GRAVITY_CSV, yLabel = "Gravity", dimens = 1, toDraw = SinglePlot.LEGEND | SinglePlot.Y_LABEL | SinglePlot.Y_NUMS, lines = constLines, legend = constLegend)
	mass = SinglePlot(id = ids[6], yLabel = "Bob Mass", dimens = 1, toDraw = SinglePlot.Y_LABEL | SinglePlot.Y_NUMS, lines = constLines)
	pendulumLength = SinglePlot(id = ids[7], yLabel = "Pendulum Length", dimens = 1, toDraw = SinglePlot.Y_LABEL | SinglePlot.Y_NUMS | SinglePlot.X_LABEL | SinglePlot.X_NUMS, lines = constLines)

	plotters = [
		[theta3d, theta1d, gravity],
		[omega3d, omega1d, mass],
		[alpha3d, alpha1d, pendulumLength]
	]

	multiPlotter = MultiPlotter(plotters)


def main():
	global environment, bobBody

	room = Room(Consts.ROOM_DIMENS, faceColor = [0, 0, 255], faces = useCeil & Prism.TOP, shouldDraw = Prism.VERTICES | Prism.EDGES)

	bobRotation = Consts.INIT_THETA

	# Get the bob position and subtract it from the center of the top of the room
	bobPos = np.subtract(np.array([0, 0, Consts.ROOM_DIMENS[2]/2]), Utils.polarToCartesian(Consts.ARM_LENGTH, Consts.INIT_TRIG))

	# Apply a perminant transformation to move the bob down, essentially setting the position as the top corner, not bottom center
	permTrans = np.array([0, 0, -Consts.BOB_DIMENS[2]])

	bobShape = Prism(Consts.BOB_DIMENS, rotation = bobRotation, position = bobPos, faces = Prism.TOP, faceColor = [0, 255, 0], center = Prism.X | Prism.Y, permTrans = permTrans, isRadians = False, shouldDraw = Prism.EDGES | Prism.VERTICES)

	bobBody = Pendulum(bobShape, Consts.BOB_MASS, Consts.ARM_LENGTH)

	initPlotters()

	environment = Environment(name = "Pendulum", size = Consts.WINDOW_DIMENS, stepFunction = step, drawFunction = draw)

	environment.registerRoom(room)
	environment.registerBody(bobBody)

	environment.activate()


if __name__ == "__main__":
    main()
