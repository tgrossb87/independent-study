CC = python3
MAIN = singleSimulation.py
HARVEST_DATA = harvestData.py
SANITY_CHECK = sanityCheck.py

run:
	$(CC) $(MAIN)

harvest:
	$(CC) $(HARVEST_DATA)

sanityCheck:
	$(CC) $(SANITY_CHECK) $(dir) $(e)
