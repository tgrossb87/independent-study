#!/usr/bin/env python3

import numpy as np
import random
from keras.models import Sequential
from keras.models import load_model as loadModel
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import LSTM
from keras.callbacks import ModelCheckpoint
from keras.utils import np_utils as NpUtils
import os
import sys
import csv
import constants as Consts
from earlyEndCallback import EarlyEndCallback
from reconstruct import reconstruct

import argparse as ArgParser

def parseArgs():
	parser = ArgParser.ArgumentParser()
	parser.add_argument("--train", action = 'store_true', help = "This flag indicates that a new model should be trained.")
	parser.add_argument("--append", action = 'store_true', help = "This flag indicates that the supplied model should be trained for additional epochs.  Must be used with -k and -i.")
	parser.add_argument("--test", action = 'store_true', help = "This flag indicates that a model does not to be supplied.  Must be used with -k.")
	parser.add_argument("--reconstruct", action = 'store_true', help = "This flag indicates that the output should be reconstructed.  Must be used with -t.")

	parser.add_argument("--input", type = str, metavar = "input", help = "The input file to use or directory to choose a file from.")
	parser.add_argument("--output", type = str, metavar = "output", help = "The directory to output weight checkpoints to.")
	parser.add_argument("--kerasModel", type = str, metavar = "kerasModel", help = "The model file to test on.")
	parser.add_argument("--generateLength", type = int, metavar = "generateLength", default = 500, help = "The number of observations to generate.")

	parser.add_argument("--sequenceLength", type = int, metavar = "sequenceLength", default = 100, help = "The length of sequence to train on.")
	parser.add_argument("--epochs", type = int, metavar = "epochs", default = 20, help = "The number of epochs to train for.")

	parser.add_argument("--batchSize", type = int, metavar = "batchSize", default = 64, help = "The batch size to use in training.")
	parser.add_argument("--memUnits", type = int, metavar = "memUnits", default = 256, help = "The number of memory units to use in training.")
	parser.add_argument("--dropoutProb", type = float, metavar = "dropoutProb", default = 0.2, help = "The dropout probability to use in training.")
	parser.add_argument("--checkpoints", action = 'store_true', help = "This flag controls if checkpoints should be stored or not.")
	parser.add_argument("--layerCount", type = int, metavar = "layerCount", default = 2, help = "This flag controls the number of layers to add to the lstm.")
	parser.add_argument("--activation", type = str, metavar = "activationFunction", default = 'softmax', help = "The activation function of the model.")
	parser.add_argument("--loss", type = str, metavar = "lossFunction", default = 'categorical_crossentropy', help = "The loss function of the model.")
	parser.add_argument("--optimization", type = str, metavar = "optimizationFunction", default = 'adam', help = "The optimization function of the model.")

	return parser.parse_args()


def parseNpArray(npString):
	strVals = npString[1:-1].split()
	floatVals = [float(strVal) for strVal in strVals]
	return np.array(floatVals)


def readFileToDict(filePath):
	outputDicts = []
	with open(filePath, newline = '') as csvFile:
		reader = csv.DictReader(csvFile, delimiter = ',', quotechar = '"', quoting = csv.QUOTE_ALL)

		for row in reader:
			parsedDict = {}
			for varName, varVal in row.items():
				if varVal[0] == "[":
					parsedDict[varName] = parseNpArray(varVal)
				else:
					parsedDict[varName] = float(varVal)
			outputDicts.append(parsedDict)
	return outputDicts


def outputToIndex(output, n, thetaMax):
	# Multiply the output by 10^n and round to an int
	# Map this from [-thetaMax * 10^n, thetaMax * 10^n] to [0, 2*thetaMax * 10^n]
	return int(round(output * (10 ** n)) + (thetaMax * (10 ** n)))


def indexToOutput(index, n, thetaMax):
#	print("Index: ", index)
	# Map the index from [0, 2*thetaMax * 10^n] to [-thetaMax * 10^n, thetaMax * 10^]
	# Then divide the index by 10^n
	return float(index - (thetaMax * (10 ** n))) / float(10 ** n)


# Should produce a list of sequences (2d) and a list of outputs (1d)
def sequentializeList(data, seqLen):
	inputs, outputs = [], []

	# Iterate through the data list and append each chunk to the inputs and
	# the value following the chunk to the outputs
	for c in range(len(data) - seqLen):
		inputs.append(data[c : c + seqLen])
		outputs.append(data[c + seqLen])
	return (inputs, outputs)


def sequentializeFile(filePath, seqLen, log = True):
	obs = readFileToDict(filePath)

	# Keep only the xts from each observation's theta
	xts = [ob[Consts.THETA_3D_CSV][0] for ob in obs]
	zt = obs[0][Consts.THETA_3D_CSV][2]

	inputs, outputs = sequentializeList(xts, seqLen)

	if log:
		print("Total patterns for file '" + filePath + "':", len(inputs))

	# Reshape for keras
	# Inputs is sequenceCount x sequenceLength
	# Reshape it to be sequenceCount x sequenceLength x 1
	shapedIns = np.reshape(inputs, (len(inputs), seqLen, 1))

	# Normalize
	# First find the max and mins of the data
	# Then transform from [min, max] to [0, 1]
	minXt, maxXt = None, None
	for xt in xts:
		if minXt == None or xt < minXt:
			minXt = xt
		if maxXt == None or xt > minXt:
			maxXt = xt
	shapedIns = (shapedIns - minXt) / float(maxXt - minXt)

	# Transform the outputs to a one hot vec encoding
	# Use the fact that the maximum xt is 60 degrees (by the harvesting function) and that
	# we only have 8 decimal points in the csv - which we will truncate to 3 (good enough)
	# Means that there are 120*(10^3) elements in the oneHotOut encoding
	oneHotOuts = np.zeros((shapedIns.shape[0], 120000))
	row = 0
	for output in outputs:
		index = outputToIndex(output, 3, 60)
		oneHotOuts[row][index] = 1
		row += 1

#	oneHotOuts = NpUtils.to_categorical(outputs)
#	print("OneHotOuts:")
#	print(oneHotOuts.shape)

#	print("ShapedIns shape:")
#	print(shapedIns.shape)

	return (zt, shapedIns, oneHotOuts, minXt, maxXt)


def createModel(sequenceLength, memUnits, dropoutProb, layerCount, activation, loss, optimizer, log = True):
	model = Sequential()
	model.add(LSTM(memUnits, input_shape = (sequenceLength, 1), return_sequences = True))
	model.add(Dropout(dropoutProb))
	for c in range(layerCount-1):
		if c == layerCount - 2:
			model.add(LSTM(memUnits))
		else:
			model.add(LSTM(memUnits, return_sequences = True))
		model.add(Dropout(dropoutProb))
	model.add(Dense(120000, activation = activation))
	model.compile(loss = loss, optimizer = optimizer)
	return model

def createCheckpoint(outputDir):
	checkpointPath = os.path.join(outputDir, "weights-{epoch:02d}-{loss:.4f}.hdf5")
	checkpoint = ModelCheckpoint(checkpointPath, monitor = 'loss', verbose = 1, save_best_only = True, mode = "min")
	return checkPoint


#def saveModel(model, modelName, outputDir):
#	saveFile = os.path.join(outputDir, "model-" + fileBase + ".hdf5")
#	model.save(saveFile)
#	return saveFile


#def fitModel(model, filePath, outputDir, seqLen, epochs, batchSize, memUnits, dropoutProb, useCheckpoints, earlyEndCallback, layerCount, activation = 'softmax', loss = 'categorical_crossentropy', optimizer = 'adam', log = True):
#	zt, shapedIns, minX, maxX = sequentializeFile(filePath, seqLen, log)
#
#	checkpoint, fileBase = createCheckpoint(filePath, outputDir)
#	earlyEndCallback.setSaveModel(saveModel)
#	earlyEndCallback.setFileBase(fileBase)
#	earlyEndCallback.setOutputDir(outputDir)
#
#	callbacks = [earlyEndCallback]
#	if useCheckpoints:
#		callbacks.append(checkpoint)
#
#	model.fit(shapedIns, shapedIns, epochs = epochs, batch_size = batchSize, callbacks = callbacks)
#
#	saveFile = saveModel(model, fileBase, outputDir)
#	return (model, saveFile)


def fitModel(model, inputBase, outputDir, epochs, batchSize, useCheckpoints, log = True):
	sequenceLength = model.input_shape[1]
	zt, shapedIns, oneHotOuts, minX, maxX = sequentializeFile(inputBase + ".csv", sequenceLength, log)

	callbacks = []
	if useCheckpoints:
		checkpoint = createCheckpoint(outputDir)
		callbacks.append(checkpoint)

	model.fit(shapedIns, oneHotOuts, epochs = epochs, batch_size = batchSize, callbacks = callbacks)


def predict(model, seedingPath, outputBase, generateLength, log = True):
	sequenceLength = model.input_shape[1]
	zt, shapedIns, oneHotOuts, minX, maxX = sequentializeFile(seedingPath + ".csv", sequenceLength, log)
	pattern = shapedIns[0]

	print("Initial pattern: ")
	print(pattern)

	simOutputPath = outputBase + ".csv"

	with open(simOutputPath, "w") as csvFile:
		writer = csv.DictWriter(csvFile, delimiter = ",", quotechar = '"', quoting = csv.QUOTE_ALL, fieldnames = [Consts.TIME_CSV, Consts.THETA_3D_CSV, Consts.OMEGA_3D_CSV, Consts.ALPHA_3D_CSV])
		writer.writeheader()

		for c in range(generateLength):
			x = np.reshape(pattern, (1, sequenceLength, 1))
			x = (x - minX) / float(maxX - minX)
			prediction = model.predict(x, verbose = 0)
			prediction = indexToOutput(np.argmax(prediction), 3, 60)
			vals = {Consts.TIME_CSV: str(c), Consts.THETA_3D_CSV: "[" + str(prediction) + " 0. " + str(zt) + "]", Consts.OMEGA_3D_CSV: "[0. 0. 0.]", Consts.ALPHA_3D_CSV: "[0. 0. 0.]"}
			writer.writerow(vals)

			pattern = np.append(pattern, [[prediction]], 0)
			pattern = np.delete(pattern, 0, 0)

	metaPath = seedingPath + "." + Consts.META_VARS_CSV + ".csv"
	metas = readFileToDict(metaPath)[0]

	with open(outputBase + "." + Consts.META_VARS_CSV + ".csv", "w") as csvFile:
		fieldNames = metas.keys()
		writer = csv.DictWriter(csvFile, delimiter = ",", quotechar = '"', quoting = csv.QUOTE_ALL, fieldnames = fieldNames)
		writer.writeheader()
		writer.writerow(metas)


def cleanInputPath(path):
	dir, file = os.path.split(path)
	if ("." + Consts.META_VARS_CSV + ".csv") in file:
		file = file[:-1 * len("." + Consts.META_VARS_CSV + ".csv")]
	elif ".csv" in file:
		file = file[:-4]
	return os.path.join(dir, file)


def standardInputFileHandling(input):
	# An input must be given
	if input is None:
		print("Error: An input file or directory must be defined.")
		sys.exit()

	# If the input is a directory, pick a random csv file from it
	# Store this randomly chosen csv file in input
	if os.path.isdir(input):
		inFile = random.choice([file for file in os.listdir(input) if not file.endswith(".meta.csv") and file.endswith(".csv")])
		print("Selected '" + inFile + "' from directory '" + input + "'")
		input = os.path.join(input, inFile)

	# Now clean the input path to a file base, again, storing it back in input
	input = cleanInputPath(input)

	# Make sure that the base + ".csv" exists
	if not os.path.isfile(input + ".csv"):
		print("Error: Invalid input file '" + input + ".csv'");
		sys.exit()

	# input should now safely point to an input file
	return input


def train(input, output, args, log = True):
	# Handle the input file as usual
	input = standardInputFileHandling(input)

	# Check for an output directory, and interpret one if it is not passed
	# Also check that this name does not collide
	if output is None:
		output = "weights-e%i-s%i-b%i-m%i-d%f-l%i" % (args.epochs, args.sequenceLength, args.batchSize, args.memUnits, args.dropoutProb, args.layerCount)
		print("Interpreting output directory as '" + output + "'")
		if os.path.exists(output) and not (os.path.isdir(output) and len(os.listdir(output)) == 0):
			print("Error: Interpretted directory name collides with existing file or directory.")
			sys.exit()

	# The output directory already exists, it must be a directory and be empty
	if os.path.exists(output) and (not os.path.isdir(output) or len(os.listdir(output)) > 0):
		print("Error: Output directory '" + output + "' must be an empty or non-existant directory.")
		sys.exit()

	# Make the output directory if it doesn't exist already
	if not os.path.exists(output):
		print("Output directory '" + output + "' has been automatically created")
		os.mkdir(output)

	# output should now safely point to an empty output directory

	print()
	print("============================== Training parameter sanity check ==============================")
	print("||")
	print("||  Input file:", input, "  Output directory:", output)
	print("||  Epochs:", args.epochs, "  Sequence Length:", args.sequenceLength, "  Dropout probability:", args.dropoutProb)
	print("||  Batch size:", args.batchSize, "  Memory units:", args.memUnits, "  Layer count:", args.layerCount)
	print("||  Activation function:", args.activation, "  Loss function:", args.loss, "  Optimization function:", args.optimization)
	print("||")
	print("=============================================================================================")
	print()

	# Create a model from the parameters
	model = createModel(args.sequenceLength, args.memUnits, args.dropoutProb, args.layerCount, args.activation, args.loss, args.optimization, log)

	# Fit this model with the input data
	fitModel(model, input, output, args.epochs, args.batchSize, args.checkpoints, log)

	# Save the model file to the output directory with a concise name
	saveFilePath = os.path.join(output, "model.hdf5")
	model.save(saveFilePath)

	# Return the model and save file path
	return (model, saveFilePath)


def append(kerasModel, input, args, log = True):
	# Handle the input file in the standard way
	input = standardInputFileHandling(input)

	# The keras model file must be an existing .hdf5 file
	if not os.path.isfile(kerasModel):
		print("Error: Keras model file '" + kerasModel + "' does not exist.")
		sys.exit()
	if not (kerasModel[-5:] == ".hdf5"):
		print("Error: Keras model file '" + kerasModel + "' must be an hdf5 file.")
		sys.exit()

	# kerasModel should now be a safe keras model file

	# Get the model from the save file
	model = loadModel(kerasModel)

	# Fit this model with the new input data)
	# Assume that outputs should go to the safe directory as the keras model
	outputDir = os.path.split(kerasModel)[0]
	fitModel(model, input, outputDir, args.epochs, args.batchSize, args.checkpoints, log)

	# Save the model file to the save output file
	model.save(kerasModel)

	# Return the model and save file path
	return (model, kerasModel)


def test(kerasModel, input, output, args, log = True):
	# Handle the input file in the standard way
	input = standardInputFileHandling(input)

	# The keras model file must be an existing .hdf5 file
	if not os.path.isfile(kerasModel):
		print("Error: Keras model file '" + kerasModel + "' does not exist.")
		sys.exit()
	if not kerasModel[-5:] == ".hdf5":
		print("Error: Keras model file '" + kerasModel + "' must be an hdf5 file.")
		sys.exit()

	# kerasModel should now be a safe keras model file

	# If output is not given, point it to the directory of the keras model
	if output is None:
		output = os.path.split(kerasModel)[0]

	# If output is an existing file, throw an error
	if os.path.isfile(output):
		print("Error: I will not write over '" + output + "'.")
		sys.exit()

	# If output is an existing directory, a default name should be used that does not write over any others
	if os.path.isdir(output):
		n = 1
		while os.path.exists(os.path.join(output, "output" + str(n) + ".csv")):
			n += 1
		output = os.path.join(output, "output" + str(n))

	# output should be a path to a non-existant file not, so clean it like an input file of .csv endings
	output = cleanInputPath(output)

	# output should now be a path to a non-existant output base

	print("============================== Testing parameter sanity check ==============================")
	print("||")
	print("|| Testing the model at:", kerasModel)
	print("|| Using a training stem from:", input)
	print("|| Writing testing outputs to:", output)
	print("|| Generating sequences of length:", args.generateLength)
	print("||")
	print("============================================================================================")


	# Get the model from the keras save file
	model = loadModel(kerasModel)

	# Test this model
	predict(model, input, output, args.generateLength, log)

	# Return the recreated model and the generated base
	return (model, output)


def main():
	args = parseArgs()

	if args.train:
		print("Training...")
		model, saveFilePath = train(args.input, args.output, args)

		if args.test:
			print("Testing post training...")
			model, outputBase = test(saveFilePath, args.input, args.output, args)

			if args.reconstruct:
				print("Reconstructing...")
				reconstruct(outputBase)
	elif args.append:
		print("Appending...")
		model, saveFilePath = append(args.input, args.output, args)

		if args.test:
			print("Testing post append...")
			model, outputBase = test(saveFilePath, args.input, args.output, args)

			if args.reconstruct:
				print("Reconstructing...")
				reconstruct(outputBase)
	elif args.test:
		print("Testing without training...")
		model, outputBase = test(args.kerasModel, args.input, args.output, args)

		if args.reconstruct:
			print("Reconstructing...")
			reconstruct(outputBase)

#	shouldTrain = not args.test or args.kerasModel is None or not os.path.isfile(args.kerasModel)
#	shouldTest = args.test
#
#	print("Training?", "Yes" if shouldTrain else "No", "  Testing?", "Yes" if shouldTest else "No")
#
#	if args.input is None:
#		print("Error: The input file must be defined")
#		sys.exit()
#
#	if os.path.isdir(args.input):
#		inFile = random.choice([file for file in os.listdir(args.input) if not file.endswith(".meta.csv")])
#		args.input = os.path.join(args.input, inFile)
#
#	if shouldTest and not os.path.isfile(args.input):
#		print("Error: The input file used to create a model must be included when testing it.")
#		sys.exit()
#
#	if args.output is None:
#		args.output = "weights-e%i-s%i-b%i-m%i-d%f-l%i" % (args.epochs, args.sequenceLength, args.batchSize, args.memUnits, args.dropoutProb, args.layerCount)
#
#	if shouldTrain:
#		if not os.path.exists(args.output):
#			print("Warning: Output directory '" + args.output + "' has been automatically created")
#			os.mkdir(args.output)
#		if not os.path.isdir(args.output):
#			print("Error: Output must be a directory (received: '" + args.output + "')")
#			sys.exit()
#		if not len(os.listdir(args.output)) == 0 and not shouldTest:
#			print("Warning: Output directory '" + args.output + "' is not empty")
#	if shouldTest:
#		if args.input is None:
#			print("Error: Input file must be defined when testing")
#			sys.exit()
#
#		if os.path.isfile(args.output):
#			print("Error: Output file '" + args.output + "' already exists")
#			sys.exit()
#
#		if args.output is None:
#			testFileOut = "output.csv"
#		elif os.path.isdir(args.output):
#			testFileOut = os.path.join(args.output, "output.csv")
#		else:
#			testFileOut = "output.csv"
#
#		if ".csv" not in testFileOut:
#			testFileOut += ".csv"
#		elif ("." + Consts.META_VARS_CSV + ".csv") in testFileOut:
#			testFileOut = testFileOut[:-1 * len("." + Consts.META_VARS_CSV + ".csv")] + ".csv"
#
#	print()
#
#	earlyEndCallback = EarlyEndCallback(saveModel)
#
#	if shouldTrain:
#		print("Selected file '" + args.input + "' with output weights going to '" + args.output + "'.")
#		print("Training for", args.epochs, "epochs on", args.sequenceLength, "observation long sequences with a", args.dropoutProb, "dropout probability.")
#		print("Using a batch size of", args.batchSize, "and", args.memUnits, "memory units on a", args.layerCount, "-layered network.")
#		print()
#
#		try:
#		model, saveFile = fitModel(args.input, args.output, args.sequenceLength, args.epochs, args.batchSize, args.memUnits, args.dropoutProb, args.checkpoints, earlyEndCallback, args.layerCount)
#		except KeyboardInterrupt:
#			earlyEndCallback.setExitFlag()
#
#		args.kerasModel = saveFile
#		print("Saved model to '" + saveFile + "'")
#
#	if shouldTest:
#		print()
#		print("Testing model defined in file '" + args.kerasModel + "'.")
#		print("Using the input file '" + args.input + "'.")
#		fileBase = predict(args.kerasModel, args.sequenceLength, args.generateLength, args.input, testFileOut, os.path.split(args.kerasModel)[0])
#		print("Wrote generated files with the base '" + fileBase + "'.")
#
#		if args.reconstruct:
#			print("Reconstructing generated file.")
#			reconstruct(fileBase)


if __name__ == "__main__":
	main()
