import csv
import os

import numpy as np
import random
import math
import decimal

import constants as Consts
import utils as Utils

from drawing.physicalBody import Pendulum
from drawing.primatives.prism import Prism

simMax = 100
outputDir = 'data'

class Simulation:
	@staticmethod
	def nameGenerator(gravity, theta, mass, length):
		sep = "$"
		return str(gravity) + sep + str(theta[0]) + sep + str(theta[2]) + sep + str(mass) + sep + str(length)


	def __init__(self, gravity, theta, mass, length, outputDir = "data", outputFileName = None):
		self.theta = theta.copy()
		self.thetaR = np.array([math.radians(ax) for ax in self.theta])
		self.trig = Utils.constructTrigsDef(self.thetaR)
		self.gravity = gravity
		self.mass = mass
		self.length = length
		self.outputDir = outputDir
		self.outputFileName = outputFileName if outputFileName is not None else Simulation.nameGenerator(self.gravity, self.theta, self.mass, self.length)


	def simulate(self, write = True):
		bobRotation = self.theta

		# Get the bob position and subtract it from the center of the top of the room
		bobPos = np.subtract(np.array([0, 0, Consts.ROOM_DIMENS[2]/2]), Utils.polarToCartesian(self.length, self.trig))

		# Apply a perminant transformation to move the bob down, essentially setting the position as the top corner, not bottom center
		permTrans = np.array([0, 0, -Consts.BOB_DIMENS[2]])

		bobShape = Prism(Consts.BOB_DIMENS, rotation = bobRotation, position = bobPos, faces = Prism.NONE, center = Prism.X | Prism.Y, permTrans = permTrans, shouldDraw = Prism.NONE, isRadians = False)

		bobBody = Pendulum(bobShape, self.mass, self.length, gravity = self.gravity)


		# Step the pendulum continuously
		# Record data at each frame and stop when the time exceeds the simulation length
		time = 0
		with open(os.path.join(self.outputDir, self.outputFileName + ".csv"), 'w') as csvFile:
			fieldNames = [Consts.TIME_CSV] + Pendulum.getValueIds(False)
			writer = csv.DictWriter(csvFile, delimiter = ",", quotechar = '"', quoting = csv.QUOTE_ALL, fieldnames = fieldNames)
			writer.writeheader()

			while time < Consts.SIM_LENGTH:
				# Step the pendulum
				bTheta, bOmega, bAlpha = bobBody.step()

				# Store the variables
				bobVals = {
					Consts.TIME_CSV: time,
					Consts.THETA_3D_CSV: bTheta,
					Consts.OMEGA_3D_CSV: bOmega,
					Consts.ALPHA_3D_CSV: bAlpha
				}
				writer.writerow(bobVals)

				# Step the time
				time += 1

		# Now, write the meta vars to the meta vars file
		with open(os.path.join(self.outputDir, self.outputFileName + "." + Consts.META_VARS_CSV + ".csv"), 'w') as csvFile:
			constNames = [Consts.GRAVITY_CSV, Consts.MASS_CSV, Consts.ARM_LENGTH_CSV, Consts.INIT_THETA_CSV, Consts.INIT_THETA_R_CSV]
			constVals = [self.gravity, self.mass, self.length, self.theta, self.thetaR]

			writer = csv.DictWriter(csvFile, delimiter = ",", quotechar = '"', quoting = csv.QUOTE_ALL, fieldnames = constNames)

			writer.writeheader()
			writer.writerow({constNames[c]: constVals[c] for c in range(len(constNames))})

		del bobRotation
		del bobPos
		del permTrans
		del bobShape
		del bobBody
		del time

		return True


	def __del__(self):
		del self.theta
		del self.thetaR
		del self.trig
		del self.gravity
		del self.mass
		del self.length
		del self.outputDir
		del self.outputFileName


def drange(x, y, step):
	x, y = decimal.Decimal(str(x)), decimal.Decimal(str(y))
	while x <= y:
		yield float(x)
		x += decimal.Decimal(step)


def simulateRepeated():
	# This is just for beautiful printing
	spinnerStates = ["-", "\\", "|", "/"]
	# These are list of all posible values of environment variables for simulation
	gravityRange = list(drange(0.0005, 0.05, '0.0005'))
	xtRange = list(range(25, 60+1))
	ztRange = list(range(-30, 30+1))
	massRange = list(range(1, 10+1))
	lengthRange = list(range(100, 300))

	combosLength = len(gravityRange) * len(xtRange) * len(ztRange) * len(massRange) * len(lengthRange)
	print()
	print("%i simulations out of %i possibities will be simulated" % (simMax, combosLength))
	print("Output data will be writen to the '%s' directory" % outputDir)
	print()

	# Generate simMax unique variable sets from this data
	varCombos = set()
	maxStr = str(combosLength)
	spinnerC = 0

	while len(varCombos) < simMax:
		grav = random.choice(gravityRange)
		xt = random.choice(xtRange)
		zt = random.choice(ztRange)
		mass = random.choice(massRange)
		length = random.choice(lengthRange)
		varCombo = (grav, xt, zt, mass, length)

		if varCombo not in varCombos:
			varCombos.add(varCombo)

		spinnerState = spinnerStates[spinnerC % len(spinnerStates)] if len(varCombos) < simMax else "Done"
		spinnerC += 1
		print("\r[%s] Generating variable combinations %.2f %% complete (%d / %d)" % (spinnerState, 100 * float(len(varCombos)) / float(simMax), len(varCombos), simMax), end = "")


	print()

	maxStr = str(simMax)
	simCount = 0
	for grav, xt, zt, mass, length in varCombos:
		simulation = Simulation(grav, np.array([xt, 0., zt]), mass, length, outputDir = outputDir)
		simulation.simulate()
		del simulation

		simCount += 1

		spinnerState = spinnerStates[simCount % len(spinnerStates)] if simCount < simMax else "Done"
		print("\r[%s] Simulating %.2f %% complete (%d / %d)" % (spinnerState, 100 * float(simCount) / float(simMax), simCount, simMax), end = "")

	print("\n\nFinished!")



if __name__ == "__main__":
    simulateRepeated()
