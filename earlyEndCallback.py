from keras.callbacks import Callback
import sys

class EarlyEndCallback(Callback):
	def __init__(self, saveModel, fileBase = "", outputDir = ""):
		self.saveModel = saveModel
		self.fileBase = fileBase
		self.outputDir = outputDir
		self.exit = False


	def setSaveModel(self, saveModel):
		self.saveModel = saveModel


	def setFileBase(self, fileBase):
		self.fileBase = fileBase


	def setOutputDir(self, outputDir):
		self.outputDir = outputDir


	def setExitFlag(self, value = True):
		self.exit = True


	def on_batch_end(self, batch, logs={}):
		if self.exit:
			self.saveMode(self.model)
			sys.exit()
