## A Data-Driven Approach to Complex Physical Simulation with Transfer Learning and Long Short Term Memory Networks
Or, in other words, my independant study.  Yay!

This project is dedicated to the developement of a regression model capable of predicting physical interactions.
Specifically, this will be the home for data (in the form of `csv` files) and code.

# Training
The `lstm.py` script is used for everything machine learning about this project.  By default, it can be used to train a model on a generated data file.  Training 
requires only one flag, but many can be used to control the parameters of the model.  These flags are as follows:

Flag | Required? | Default | Meaning
:---: | :---: | :---: | ---
`-i` | Yes | None | The input file to train on. If this is a directory (ex. `data`) then a random `csv` file will be chosen from the specified directory.
`-o` | No | Interpreted | The output directory to stream checkpoints and model files to. If this is not specified, one will be created based on the parameters of the model.
`-c` | No | False | This flag controls if checkpoints should be saved. If this is set to true, checkpoints will be saved for every epoch where the loss decreases.
`-l` | No | 2 | The number of LSTM layers to use.
`-s` | No | 100 | The length of the sequence to train on. This controls how long each training observation is.
`-e` | No | 20 | The number of epochs to train for.
`-b` | No | 64 | The size of the batch to use. Smaller batches will result in longer epochs.
`-m` | No | 256 | The number of memory units to include in each LSTM layer.
`-d` | No | 0.2 | The chance that a result will be dropped. This controls the persistance of previous results.


Additionally, the `lstm.py` script can be used for testing and optional automatic reconstruction. To test a model, these flags can be used:

Flag | Required? | Default | Meaning |
:---: | :---: | :---: | ---
`-t` | Yes | False | This flag controls if testing should happen. This must be used to begin the testing routine.
`-r` | No | False | This flag controls if the generated data should be reconstructed automatically. If used, the reconstructed output will be shown after generation.
`-k` | Yes | None | The keras model file to test. This must be used if testing is happening independent of the training routine.
`-i` | Yes | None | The input file that the model was trained on. This must be supplied to get the initial sequence for testing. It must be a specific file, not a directory.
`-s` | Yes | 100 | Although the sequence length has a default, it must be specified if it was specified when the model was trained. This must match the training sequence length.
`-g` | No | 500 | The number of predictions to generate.
`-o` | No | Interpreted | The output file to stream results to. If this is a directory, generated files will be placed here with default names. If it is a file, this file will be used as the basename for generated files. If this is not given, default names will be used and generated files will be places in the current directory.

The final use of the `lstm.py` script is for adding additional training to a trained model.  This can be used to train a model on multiple files.  The following flags can be used:

Flag | Required? | Default | Meaning
:---: | :---: | :---: | ---
`-a` | Yes | False | This flag controls if appending should happen.  This must be used to add additional training to a model.
`-k` | Yes | None | The keras model file to train additionally.
`-i` | Yes | None | The input file to train on.  This flags functions the same way as it does in the training routine.
`-s` | No | 100 | The length of the sequence to train on.  This **needs** to match the sequence length used to train the model before.
`-e` | No | 20 | The number of epochs to train for.  Not dependent on previous training instances.



A full list of all available flags can be found using `./lstm.py --help`.  However, this list does not indicate each flag's use in context of training and testing, so it is much easier to use these 
tables.  Once you become well-versed in the `lstm.py` toolkit, it is possible to use the help list to find the proper flags.

## This branch
This branch is dedicated to the development of a pendulum simulation.
